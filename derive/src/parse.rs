use syn::{
    self,
    parse::{Parse, ParseStream},
    punctuated::Punctuated,
    Attribute, Expr, Ident, LitStr, Token,
};

pub enum Attr {
    // single-identifier attributes
    Debug,
    Transparent,

    // ident = "literal"
    Name(LitStr),

    // ident = expr
    FmtWith(Expr),
}

impl Parse for Attr {
    fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
        let name: Ident = input.parse()?;
        let name_str = name.to_string();

        if input.peek(Token![=]) {
            let _token: Token![=] = input.parse()?;

            if input.peek(LitStr) {
                let lit: LitStr = input.parse()?;

                match name_str.as_str() {
                    "name" => Ok(Attr::Name(lit)),
                    _ => panic!("unsupported attribute {}", &name_str),
                }
            } else {
                match input.parse::<Expr>() {
                    Ok(expr) => match name_str.as_str() {
                        "fmt_with" => Ok(Attr::FmtWith(expr)),
                        _ => panic!("unsupported attribute {}", &name_str),
                    },

                    Err(_) => panic!("expected `string literal` or `expression` after `=`"),
                }
            }
        } else {
            match name_str.as_str() {
                "debug" => Ok(Attr::Debug),
                "transparent" => Ok(Attr::Transparent),
                _ => panic!("unsupported attribute {}", &name_str),
            }
        }
    }
}

pub fn parse_attributes(all_attrs: &[Attribute]) -> Vec<Attr> {
    all_attrs
        .iter()
        .filter(|attr| attr.path().is_ident("display"))
        .flat_map(|attr| {
            attr.parse_args_with(Punctuated::<Attr, Token![,]>::parse_terminated)
                .expect("malformed attributes")
        })
        .collect()
}
