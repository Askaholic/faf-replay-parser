use crate::parse::{parse_attributes, Attr};

pub struct FieldParams {
    pub name: Option<syn::LitStr>,
    pub fmt_with: Option<syn::Expr>,
    pub debug: bool,
}
impl FieldParams {
    fn new() -> Self {
        Self {
            name: None,
            fmt_with: None,
            debug: false,
        }
    }

    pub fn from_attrs(attrs: &[syn::Attribute]) -> Self {
        let mut params = Self::new();

        for attr in parse_attributes(attrs) {
            match attr {
                Attr::Name(name) => params.name = Some(name),
                Attr::FmtWith(expr) => params.fmt_with = Some(expr),
                Attr::Debug => params.debug = true,
                _ => panic!("unsupported attribute on field"),
            }
        }

        params
    }
}

pub struct VariantParams {
    pub transparent: bool,
}
impl VariantParams {
    fn new() -> Self {
        Self { transparent: false }
    }

    pub fn from_attrs(attrs: &[syn::Attribute]) -> Self {
        let mut params = Self::new();

        for attr in parse_attributes(attrs) {
            match attr {
                Attr::Transparent => params.transparent = true,
                _ => panic!("unsupported attribute on variant"),
            }
        }

        params
    }
}
