use proc_macro::TokenStream;
use quote::quote;
use syn;

mod attr;
mod parse;

#[proc_macro_derive(Display, attributes(display))]
pub fn display_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(input as syn::DeriveInput);

    impl_display_derive(&ast)
}

fn impl_display_derive(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    let body = impl_body(name, &ast.data);

    let gen = quote! {
        impl ::core::fmt::Display for #name {
            fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> ::core::fmt::Result {
                use crate::display::DisplayBuilders;

                #body
            }
        }
    };
    gen.into()
}

fn impl_body(name: &syn::Ident, data: &syn::Data) -> proc_macro2::TokenStream {
    use syn::{Data, Fields};

    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let names = fields
                    .named
                    .iter()
                    .map(|field| field.ident.as_ref().unwrap());
                let field_calls = fields.named.iter().map(|field| {
                    let (name, display) = get_field_display(field);
                    quote! {
                        .field(#name, #display)
                    }
                });
                quote! {
                    match *self {
                        #name { #(ref #names),* } => {
                            f.display_struct()
                                .name(stringify!(#name))
                                #(#field_calls)*
                                .finish()
                        }
                    }
                }
            }
            Fields::Unnamed(ref _fields) => {
                quote!(Ok(()))
            }
            Fields::Unit => {
                quote!(Ok(()))
            }
        },
        Data::Enum(ref data) => {
            let arms = data.variants.iter().map(|var| {
                let params = attr::VariantParams::from_attrs(&var.attrs);
                let variant_name = &var.ident;

                match var.fields {
                    Fields::Named(ref fields) => {
                        let names = fields
                            .named
                            .iter()
                            .map(|field| field.ident.as_ref().unwrap());
                        let field_calls = fields.named.iter().map(|field| {
                            let (name, display) = get_field_display(field);
                            quote! {
                                .field(#name, #display)
                            }
                        });
                        quote! {
                            #variant_name { #(ref #names),* } => {
                                f.display_struct()
                                    .name(stringify!(#variant_name))
                                    #(#field_calls)*
                                    .finish()
                            }
                        }
                    }
                    Fields::Unnamed(ref _fields) => {
                        if params.transparent {
                            quote! {
                                #variant_name(ref inner) => ::core::fmt::Display::fmt(&inner, f)
                            }
                        } else {
                            quote! {
                                #variant_name(ref inner) => write!(f, concat!(stringify!(#variant_name), "({})"), inner)
                            }
                        }
                    }
                    Fields::Unit => quote! {
                        #variant_name => {
                            f.display_struct()
                                .name(stringify!(#variant_name))
                                .finish()
                        }
                    },
                }
            });
            quote! {
                match *self {
                    #(#name::#arms),*
                }
            }
        }
        Data::Union(_) => unimplemented!(),
    }
}

fn get_field_display(field: &syn::Field) -> (proc_macro2::TokenStream, proc_macro2::TokenStream) {
    let params = attr::FieldParams::from_attrs(&field.attrs);
    let field_name = field.ident.as_ref().unwrap();

    let name = match params.name {
        Some(ref name) => quote!(#name),
        None => quote!(stringify!(#field_name)),
    };

    let value = params
        .fmt_with
        .as_ref()
        .map(|fmt_with| quote!(&#fmt_with(#field_name)))
        .unwrap_or_else(|| match params.debug {
            true => quote!(&crate::display::DisplayAsDebug(#field_name)),
            false => quote!(#field_name),
        });

    (name, value)
}
