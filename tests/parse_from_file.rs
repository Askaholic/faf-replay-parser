#[cfg(test)]
use pretty_assertions::assert_eq;

use std::fs::File;
use std::io::{BufReader, Read, Seek, SeekFrom};
use std::iter::FromIterator;

use faf_replay_parser::iter::prelude::*;
use faf_replay_parser::lua;
use faf_replay_parser::parser::parse_body_ticks;
use faf_replay_parser::{sc2, scfa, Parser, ParserBuilder, ReplayReadError, SC2, SCFA};

use ordered_hash_map::OrderedHashMap;

#[test]
fn scfa_parse_header() {
    let parser = Parser::<SCFA>::new();
    let mut file = BufReader::new(File::open("tests/data/6176549.scfareplay").unwrap());

    let header = parser.parse_header(&mut file).unwrap();

    assert_eq!(header.scfa_version, "Supreme Commander v1.50.3698");
    assert_eq!(header.replay_version, "Replay v1.9");
    assert_eq!(header.map_file, "/maps/SCMP_009/SCMP_009.scmap");
    assert_eq!(
        header.players,
        OrderedHashMap::from_iter([
            ("PlodoNoob".to_string(), -1),
            ("EricaPwnz".to_string(), -1),
            ("Mizer".to_string(), -1),
            ("Vmcsnekke".to_string(), -1),
            ("HerzogGorky".to_string(), -1),
            ("Strogo".to_string(), -1),
            ("Robogear".to_string(), -1),
            ("Licious".to_string(), -1),
        ])
    );
    assert_eq!(header.cheats_enabled, false);
    assert_eq!(
        header.armies,
        OrderedHashMap::from_iter([
            (
                0,
                lua! {
                    ["MEAN"] = 2046.1973,
                    ["StartSpot"] = 1.0,
                    ["ArmyColor"] = 16.0,
                    ["PlayerClan"] = "SNF",
                    ["NG"] = 2238.0,
                    ["Team"] = 3.0,
                    ["Ready"] = true,
                    ["PlayerColor"] = 16.0,
                    ["Civilian"] = false,
                    ["PlayerName"] = "PlodoNoob",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 1800.0,
                    ["Country"] = "fr",
                    ["Faction"] = 3.0,
                    ["OwnerID"] = "139168",
                    ["ArmyName"] = "ARMY_1",
                    ["Human"] = true,
                    ["BadMap"] = false,
                    ["DEV"] = 92.498055,
                }
            ),
            (
                1,
                lua! {
                    ["MEAN"] = 1492.6638,
                    ["StartSpot"] = 2.0,
                    ["ArmyColor"] = 8.0,
                    ["PlayerClan"] = "PLD",
                    ["NG"] = 309.0,
                    ["Team"] = 2.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "EricaPwnz",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = 1.0,
                    ["PL"] = 1200.0,
                    ["OwnerID"] = "240734",
                    ["Country"] = "ru",
                    ["Faction"] = 2.0,
                    ["ArmyName"] = "ARMY_2",
                    ["Human"] = true,
                    ["PlayerColor"] = 8.0,
                    ["DEV"] = 95.28792,
                }
            ),
            (
                2,
                lua! {
                    ["MEAN"] = 1655.6,
                    ["StartSpot"] = 3.0,
                    ["ArmyColor"] = 12.0,
                    ["PlayerClan"] = "",
                    ["NG"] = 2057.0,
                    ["Team"] = 3.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "Mizer",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 1400.0,
                    ["OwnerID"] = "117425",
                    ["Country"] = "us",
                    ["Faction"] = 4.0,
                    ["ArmyName"] = "ARMY_3",
                    ["Human"] = true,
                    ["PlayerColor"] = 12.0,
                    ["DEV"] = 94.7987,
                }
            ),
            (
                3,
                lua! {
                    ["MEAN"] = 1893.34,
                    ["StartSpot"] = 4.0,
                    ["ArmyColor"] = 17.0,
                    ["PlayerClan"] = "",
                    ["NG"] = 3237.0,
                    ["Team"] = 2.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "Vmcsnekke",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 1600.0,
                    ["OwnerID"] = "1413",
                    ["Country"] = "nl",
                    ["Faction"] = 1.0,
                    ["ArmyName"] = "ARMY_4",
                    ["Human"] = true,
                    ["PlayerColor"] = 17.0,
                    ["DEV"] = 96.6439,
                }
            ),
            (
                4,
                lua! {
                    ["MEAN"] = 1726.1464,
                    ["StartSpot"] = 5.0,
                    ["ArmyColor"] = 6.0,
                    ["PlayerClan"] = "SC",
                    ["NG"] = 530.0,
                    ["Team"] = 3.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "HerzogGorky",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 1400.0,
                    ["OwnerID"] = "225417",
                    ["Country"] = "ru",
                    ["Faction"] = 3.0,
                    ["ArmyName"] = "ARMY_5",
                    ["Human"] = true,
                    ["PlayerColor"] = 6.0,
                    ["DEV"] = 94.46352,
                }
            ),
            (
                5,
                lua! {
                    ["MEAN"] = 2352.03,
                    ["StartSpot"] = 6.0,
                    ["ArmyColor"] = 2.0,
                    ["PlayerClan"] = "AoS",
                    ["NG"] = 1647.0,
                    ["Team"] = 2.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "Strogo",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 2100.0,
                    ["OwnerID"] = "239",
                    ["Country"] = "by",
                    ["Faction"] = 1.0,
                    ["ArmyName"] = "ARMY_6",
                    ["Human"] = true,
                    ["PlayerColor"] = 2.0,
                    ["DEV"] = 91.5646,
                }
            ),
            (
                6,
                lua! {
                    ["MEAN"] = 2330.7153,
                    ["StartSpot"] = 7.0,
                    ["ArmyColor"] = 1.0,
                    ["PlayerClan"] = "JEW",
                    ["NG"] = 1542.0,
                    ["Team"] = 3.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "Robogear",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 2100.0,
                    ["OwnerID"] = "147260",
                    ["Country"] = "ru",
                    ["Faction"] = 1.0,
                    ["ArmyName"] = "ARMY_7",
                    ["Human"] = true,
                    ["PlayerColor"] = 1.0,
                    ["DEV"] = 90.84315,
                }
            ),
            (
                7,
                lua! {
                    ["MEAN"] = 1920.26,
                    ["StartSpot"] = 8.0,
                    ["ArmyColor"] = 11.0,
                    ["PlayerClan"] = "JEW",
                    ["NG"] = 3378.0,
                    ["Team"] = 2.0,
                    ["Ready"] = true,
                    ["BadMap"] = false,
                    ["Civilian"] = false,
                    ["PlayerName"] = "Licious",
                    ["AIPersonality"] = "",
                    ["ObserverListIndex"] = -1.0,
                    ["PL"] = 1600.0,
                    ["OwnerID"] = "41272",
                    ["Country"] = "dk",
                    ["Faction"] = 4.0,
                    ["ArmyName"] = "ARMY_8",
                    ["Human"] = true,
                    ["PlayerColor"] = 11.0,
                    ["DEV"] = 98.7309,
                }
            ),
            (
                255,
                lua! {
                    ["PlayerColor"] = 1.0,
                    ["Civilian"] = true,
                    ["StartSpot"] = 1.0,
                    ["AIPersonality"] = "",
                    ["ArmyColor"] = 1.0,
                    ["ArmyName"] = "NEUTRAL_CIVILIAN",
                    ["Faction"] = 5.0,
                    ["Team"] = 1.0,
                    ["Human"] = false,
                    ["PlayerName"] = "civilian",
                    ["Ready"] = false,
                }
            ),
        ])
    );
    assert_eq!(header.seed, 22557011);
}

#[test]
fn scfa_parse_default() {
    let parser = Parser::<SCFA>::new();
    let mut file = BufReader::new(File::open("tests/data/6176549.scfareplay").unwrap());

    let replay = parser.parse(&mut file).unwrap();
    assert_eq!(
        replay.body.commands.last().unwrap(),
        &scfa::replay::ReplayCommand::EndGame,
    );
    assert_eq!(replay.body.commands.len(), 260421);
}

#[test]
fn sc2_parse_default() {
    let parser = Parser::<SC2>::new();
    let mut file = BufReader::new(File::open("tests/data/other_best_win.sc2replay").unwrap());

    let replay = parser.parse(&mut file).unwrap();
    assert_eq!(
        replay.body.commands.last().unwrap(),
        &sc2::replay::ReplayCommand::EndGame,
    );
    assert_eq!(replay.body.commands.len(), 118777);
}

#[test]
fn scfa_parse_all() {
    let parser = ParserBuilder::<SCFA>::new().commands_all().build();
    let file = File::open("tests/data/6176549.scfareplay").unwrap();
    let mut reader = BufReader::new(file);

    let replay = parser.parse(&mut reader).unwrap();

    assert_eq!(
        replay.body.commands.last().unwrap(),
        &scfa::replay::ReplayCommand::EndGame,
    );
    assert_eq!(replay.body.commands.len(), 310919);
}

#[test]
fn sc2_parse_all() {
    let parser = ParserBuilder::<SC2>::new().commands_all().build();
    let file = File::open("tests/data/other_best_win.sc2replay").unwrap();
    let mut reader = BufReader::new(file);

    let replay = parser.parse(&mut reader).unwrap();

    assert_eq!(
        replay.body.commands.last().unwrap(),
        &sc2::replay::ReplayCommand::EndGame,
    );
    assert_eq!(replay.body.commands.len(), 128042);
}

#[test]
fn scfa_stream_parse_all() {
    let builder = ParserBuilder::<SCFA>::new().commands_all();
    let parser = builder.build_clone();
    let mut stream = builder.build_stream();
    let file = File::open("tests/data/6176549.scfareplay").unwrap();
    let mut reader = BufReader::new(file);

    let replay1 = parser.parse(&mut reader).unwrap();

    assert_eq!(
        replay1.body.commands.last().unwrap(),
        &scfa::replay::ReplayCommand::EndGame
    );

    reader.seek(SeekFrom::Start(0)).unwrap();
    let replay2 = {
        while stream.feed_reader(&mut reader, 8192).unwrap() != 0 {
            match stream.parse() {
                Err(ReplayReadError::IO(err)) => {
                    assert_eq!(err.kind(), std::io::ErrorKind::UnexpectedEof)
                }
                res => res.unwrap(),
            }
        }
        assert!(stream.can_finalize());
        stream.finalize().unwrap()
    };

    assert_eq!(
        replay2.body.as_ref().unwrap().commands.last().unwrap(),
        &scfa::replay::ReplayCommand::EndGame
    );
    assert_eq!(
        replay1.body.commands.len(),
        replay2.body.unwrap().commands.len()
    );
}

#[test]
fn scfa_parse_all_invalid_1() {
    let parser = ParserBuilder::<SCFA>::new().commands_all().build();
    let mut f = BufReader::new(File::open("tests/data/1418712.scfareplay").unwrap());

    match parser.parse(&mut f).unwrap_err() {
        ReplayReadError::IO(e) => assert_eq!(e.kind(), std::io::ErrorKind::UnexpectedEof),
        e => panic!("Wrong error returned {:?}", e),
    }
}

#[test]
fn scfa_parse_default_desynced() {
    let parser = Parser::<SCFA>::new();
    let mut f = BufReader::new(File::open("tests/data/8748707-desynced.scfareplay").unwrap());

    match parser.parse(&mut f).unwrap_err() {
        ReplayReadError::Desynced(tick) => assert_eq!(tick, 9105),
        e => panic!("Wrong error returned {:?}", e),
    }
}

#[test]
fn scfa_parse_full_desynced() {
    let parser = ParserBuilder::<SCFA>::new()
        .commands_default()
        .stop_on_desync(false)
        .build();
    let mut f = BufReader::new(File::open("tests/data/8748707-desynced.scfareplay").unwrap());

    let replay = parser.parse(&mut f).unwrap();

    assert_eq!(replay.body.sim.desync_tick, Some(9105));
    assert_eq!(
        replay.body.sim.desync_ticks,
        Some(vec![
            9105, 9154, 9205, 9254, 9305, 9352, 9405, 9454, 9505, 9554, 9604, 9654, 9705, 9755,
            9805, 9860, 9906, 9967, 10004, 10055, 10104, 10158, 10204, 10255, 10305, 10355, 10404,
            10453, 10504, 10554, 10605, 10652, 10703, 10755, 10806, 10855, 10903, 10955, 11004,
            11054, 11105, 11155, 11206, 11258, 11304, 11353, 11404, 11455, 11504, 11554, 11606,
            11655, 11704, 11754, 11804, 11854, 11905, 11955, 12005, 12055, 12104, 12153, 12205,
            12254, 12304, 12354, 12404, 12455, 12504, 12554, 12604, 12653, 12703, 12752, 12802,
            12854, 12904, 12955, 13004, 13058, 13103, 13155, 13203, 13253, 13304, 13354, 13406,
            13455, 13504, 13554, 13605, 13654, 13704, 13756, 13804, 13855, 13903, 13954, 14005,
            14006, 14006, 14006, 14006, 14006, 14009, 14056, 14104, 14155, 14205, 14253, 14305,
            14353, 14355, 14355, 14355, 14355, 14356, 14357, 14405, 14454, 14503, 14554, 14603,
            14657, 14705, 14753, 14802, 14852, 14904, 14951, 14953, 14953, 14953, 14953, 14953,
            14954, 15005, 15059, 15103, 15155, 15202, 15253, 15304, 15353, 15404, 15452, 15452,
            15452, 15453, 15453, 15453, 15453, 15503, 15554, 15604, 15654, 15703, 15753, 15804,
            15853, 15903, 15952, 16005, 16054, 16103, 16154, 16205, 16254, 16304, 16355, 16405,
            16457, 16504, 16555, 16603, 16656, 16705, 16755, 16805, 16853, 16905, 16961, 17004,
            17055, 17104, 17156, 17202, 17254, 17305, 17354, 17404, 17454, 17505, 17554, 17603,
            17652, 17703, 17753, 17805, 17854, 17903, 17954, 18005, 18052, 18104, 18153, 18205,
            18254, 18304, 18354, 18404, 18456, 18504, 18554, 18603, 18656, 18704, 18752, 18805,
            18854, 18906, 18953, 19002, 19053, 19103, 19154, 19204, 19254, 19304, 19354, 19408,
            19453, 19502, 19555, 19605, 19653, 19653, 19653, 19653, 19653, 19653, 19656, 19705,
            19754, 19804, 19853, 19910, 19958, 20004, 20055, 20105, 20154, 20203, 20258, 20317,
            20354, 20411, 20456, 20505, 20554, 20603, 20655, 20656, 20656, 20656, 20657, 20657,
            20658
        ])
    );
}

#[test]
fn scfa_parse_ticks_only() {
    use std::convert::TryInto;

    let mut f = BufReader::new(File::open("tests/data/6176549.scfareplay").unwrap());
    let mut data = Vec::new();
    f.read_to_end(&mut data).unwrap();

    // Count the ticks with optimized functions
    let start = scfa::bytes::body_offset(&data).unwrap();
    let fast_ticks = scfa::bytes::body_ticks(&data[start..]).unwrap();
    let reader_ticks = parse_body_ticks::<SCFA>(&mut &data[start..]).unwrap();

    // Count the ticks with a byte iterator
    let iter_raw_ticks: u32 = (&data[start..])
        .iter_command_frames_raw()
        .filter(|frame| frame.cmd == scfa::replay::replay_command::ADVANCE)
        .map(|frame| u32::from_le_bytes(frame.data[3..7].try_into().unwrap()))
        .sum();

    // Count the ticks with a normal parser
    let parser = ParserBuilder::<SCFA>::new().commands_all().build();
    let replay = parser.parse(&mut data.as_slice()).unwrap();
    let parser_ticks = replay.body.sim.tick;

    assert_eq!(fast_ticks, 28917);
    assert_eq!(fast_ticks, parser_ticks);
    assert_eq!(fast_ticks, reader_ticks);
    assert_eq!(fast_ticks, iter_raw_ticks);
}
