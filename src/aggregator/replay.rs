use std::collections::HashMap;
use std::path::PathBuf;
use std::time::Duration;

/// Aggregated information extracted from the replay file in a more usable form. All attributes
/// are converted to standard Rust types.
#[derive(Debug)]
pub struct ReplayInfo {
    pub scfa_version: String,
    pub replay_version: String,
    pub seed: u32,
    pub duration: Option<Duration>,
    pub quality: Option<f32>,
    pub ranked: Option<bool>,
    pub map: ReplayMapInfo,
    pub mods: Vec<ReplayModInfo>,
    pub options: ReplayOptionsInfo,
    pub players: HashMap<String, ReplayPlayerInfo>,
}

#[derive(Debug)]
pub struct ReplayMapInfo {
    pub map_file: PathBuf,
    /// Display name
    pub name: Option<String>,
    /// Descriptive flavor text
    pub description: Option<String>,
    pub scenario_file: Option<PathBuf>,
    pub script_file: Option<PathBuf>,
    pub save_file: Option<PathBuf>,
    pub preview: Option<String>,
}

#[derive(Debug)]
pub struct ReplayModInfo {
    pub uid: Option<String>,
    pub version: Option<f32>,
    pub name: Option<String>,
    pub description: Option<String>,
    pub author: Option<String>,
    pub url: Option<String>,
    pub location: Option<PathBuf>,
    pub icon: Option<PathBuf>,
    pub copyright: Option<String>,
}

#[derive(Debug)]
pub struct ReplayOptionsInfo {
    pub title: Option<String>,
    pub quality: Option<f32>,
    pub replay_id: Option<String>,
    pub victory: Option<String>,
    pub unit_cap: Option<u32>,
    pub cheats: Option<bool>,
    pub cheat_mult: Option<f32>,
    pub build_mult: Option<f32>,
    pub share: Option<String>,
    pub score: Option<String>,
    pub game_speed: Option<String>,
    pub no_rush: Option<String>,
}

// Ignored army keys:
// ObserverListIndex
// Ready
// BadMap
#[derive(Debug)]
pub struct ReplayPlayerInfo {
    pub id: Option<usize>,
    /// Display name
    pub name: Option<String>,
    pub human: Option<bool>,
    /// Displayed rating
    pub rating: Option<i32>,
    pub mean: Option<f32>,
    pub deviation: Option<f32>,
    pub clan: Option<String>,
    pub country: Option<String>,
    pub start_spot: Option<u32>,
    // TODO: Can be negative for observer?
    pub team: Option<u32>,
    pub faction: Option<u32>,
    pub player_color: Option<u32>,
    pub army_color: Option<u32>,
    pub army_name: Option<String>,

    // AI only
    pub ai_personality: Option<String>,
    pub civilian: Option<bool>,

    // What are these?
    pub ng: Option<i32>,
    pub pl: Option<i32>,
    pub rc: Option<String>,
}
