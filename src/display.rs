/// Display builders similar to the std::fmt debug_builders but for types that implement Display.
use std::fmt;

pub trait DisplayBuilders<'a, 'b: 'a> {
    fn display_struct(&'a mut self) -> DisplayStruct<'a, 'b>;
}

impl<'a, 'b: 'a> DisplayBuilders<'a, 'b> for fmt::Formatter<'b> {
    fn display_struct(&'a mut self) -> DisplayStruct<'a, 'b> {
        DisplayStruct::new(self)
    }
}

pub struct DisplayStruct<'a, 'b: 'a> {
    fmt: &'a mut fmt::Formatter<'b>,
    result: fmt::Result,
    has_fields: bool,
}

impl<'a, 'b: 'a> DisplayStruct<'a, 'b> {
    pub fn new(fmt: &'a mut fmt::Formatter<'b>) -> Self {
        DisplayStruct {
            fmt,
            result: Ok(()),
            has_fields: false,
        }
    }

    pub fn name(&mut self, name: &str) -> &mut Self {
        self.result = self.result.and_then(|_| self.fmt.write_str(name));
        self
    }

    pub fn field(&mut self, name: &str, value: &dyn fmt::Display) -> &mut Self {
        self.result = self.result.and_then(|_| {
            let prefix = if self.has_fields { ", " } else { " { " };
            self.fmt.write_str(prefix)?;
            self.fmt.write_str(name)?;
            self.fmt.write_str(": ")?;
            value.fmt(self.fmt)
        });

        self.has_fields = true;
        self
    }

    pub fn finish(&mut self) -> fmt::Result {
        if self.has_fields {
            self.result = self.result.and_then(|_| self.fmt.write_str(" }"));
        }
        self.result
    }
}

pub struct DisplayAsDebug<T: fmt::Debug>(pub T);
impl<T: fmt::Debug> fmt::Display for DisplayAsDebug<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.0, f)
    }
}

pub struct DebugAsDisplay<T: fmt::Display>(pub T);
impl<T: fmt::Display> fmt::Debug for DebugAsDisplay<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}

pub struct HexArray<'a>(pub &'a [u8]);
impl fmt::Display for HexArray<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for byte in self.0 {
            write!(f, "{:02x}", byte)?
        }
        Ok(())
    }
}

pub struct TransparentOption<'a, T: fmt::Display>(pub &'a Option<T>);
impl<T: fmt::Display> fmt::Display for TransparentOption<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.0 {
            Some(ref inner) => fmt::Display::fmt(inner, f),
            None => f.write_str("None"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_basic() {
        struct Test {
            a: u64,
            b: String,
        }
        impl fmt::Display for Test {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.display_struct()
                    .name("Test")
                    .field("a", &self.a)
                    .field("b", &self.b)
                    .finish()
            }
        }
        let strct = Test {
            a: 10,
            b: "Hello World!".into(),
        };
        assert_eq!(format!("{}", &strct), "Test { a: 10, b: Hello World! }");
    }
}
