//! Generic struct definitions for Supreme Commander replay files.

use ordered_hash_map::OrderedHashMap;

use crate::lua::LuaObject;
use crate::version::Version;

use std::collections::HashMap;

/// Parsed representation of data as it appears in the replay file.
#[derive(Debug)]
pub struct Replay<V: Version> {
    pub header: ReplayHeader,
    pub body: ReplayBody<V>,
}

// Implemented by hand so that Version doesn't need Clone
impl<V: Version> Clone for Replay<V> {
    #[inline]
    fn clone(&self) -> Self {
        Self {
            header: self.header.clone(),
            body: self.body.clone(),
        }
    }
}

// Implemented by hand so that Version doesn't need PartialEq
impl<V: Version> PartialEq for Replay<V> {
    #[inline]
    fn eq(&self, other: &Replay<V>) -> bool {
        self.header == other.header && self.body == other.body
    }
}

/// Parsed representation of the replay header section.
#[derive(Clone, Debug, PartialEq)]
pub struct ReplayHeader {
    pub scfa_version: String,
    pub replay_version: String,
    pub map_file: String,
    pub mods: LuaObject,
    pub scenario: LuaObject,
    pub players: OrderedHashMap<String, i32>,
    pub cheats_enabled: bool,
    pub armies: OrderedHashMap<u8, LuaObject>,
    pub seed: u32,
}

/// Parsed representation of the command stream along with some incrementally computed simulation
/// data.
#[derive(Debug)]
pub struct ReplayBody<V: Version> {
    pub commands: Vec<V::Command>,
    pub sim: SimData,
}

// Implemented by hand so that Version doesn't need Clone
impl<V: Version> Clone for ReplayBody<V> {
    fn clone(&self) -> Self {
        Self {
            commands: self.commands.clone(),
            sim: self.sim.clone(),
        }
    }
}

// Implemented by hand so that Version doesn't need PartialEq
impl<V: Version> PartialEq for ReplayBody<V> {
    #[inline]
    fn eq(&self, other: &ReplayBody<V>) -> bool {
        self.commands == other.commands && self.sim == other.sim
    }
}

/// Basic simulation data that is updated as the command stream is parsed.
#[derive(Clone, Debug, PartialEq)]
pub struct SimData {
    /// The current tick
    pub tick: u32,
    /// The player id of the current command sender. Only valid if `tick > 0`
    pub command_source: u8,
    /// A map of player id's to the tick on which their last command was received
    pub players_last_tick: HashMap<u8, u32>,
    /// The current checksum value. Only valid if `checksum_tick != None`
    pub checksum: [u8; 16],
    /// The current tick which the checksum is verifying
    pub checksum_tick: Option<u32>,
    /// The first tick that was desynced
    pub desync_tick: Option<u32>,
    /// A list of all ticks that were desynced
    pub desync_ticks: Option<Vec<u32>>,
}
impl SimData {
    pub fn new() -> SimData {
        SimData {
            tick: 0,
            command_source: 0,
            players_last_tick: HashMap::new(),
            checksum: [0; 16],
            checksum_tick: None,
            desync_tick: None,
            desync_ticks: None,
        }
    }
}

/// A parsed command frame header and following raw command data.
///
/// This data will be parsed into a `Command` implementation.
#[derive(Clone, Debug, PartialEq)]
pub struct ReplayCommandFrame<V: Version> {
    pub command_id: V::CommandId,
    /// Size of the entire frame. This is data.len() + 3
    pub size: u16,
    /// Contents of the frame without the frame header.
    pub data: Vec<u8>,
}

/// A raw command frame header and reference to body data independent of any game version.
#[derive(Debug, PartialEq)]
pub struct ReplayCommandFrameSpan<'a> {
    pub cmd: u8,
    pub size: u16,
    pub data: &'a [u8],
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    use crate::scfa::replay::*;
    use crate::scfa::SCFA;

    #[test]
    fn test_struct_traits() {
        let replay = Replay::<SCFA> {
            header: ReplayHeader {
                scfa_version: "Supreme Commander v1.50.3698".to_string(),
                replay_version: "Replay v1.9".to_string(),
                map_file: "/maps/map1/map1.scmap".to_string(),
                mods: LuaObject::Nil,
                scenario: LuaObject::Nil,
                armies: OrderedHashMap::new(),
                cheats_enabled: false,
                players: OrderedHashMap::new(),
                seed: 0,
            },
            body: ReplayBody {
                sim: SimData {
                    tick: 0,
                    command_source: 0,
                    players_last_tick: HashMap::new(),
                    checksum: [0; 16],
                    checksum_tick: None,
                    desync_tick: None,
                    desync_ticks: None,
                },
                commands: vec![],
            },
        };
        let replay2 = replay.clone();

        let _ = format!("{:?}", &replay);
        assert_eq!(replay, replay2);
    }

    #[test]
    fn test_replay_command_display() {
        use crate::lua::LuaTable;
        use ReplayCommand::*;

        assert_eq!(
            format!(
                "{}",
                VerifyChecksum {
                    digest: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                    tick: 10
                }
            ),
            "VerifyChecksum { digest: 000102030405060708090a0b0c0d0e0f, tick: 10 }"
        );
        assert_eq!(
            format!(
                "{}",
                CreateProp {
                    blueprint: "abcd".into(),
                    position: Position {
                        x: 0.0,
                        y: 10.0,
                        z: -100.0
                    }
                }
            ),
            "CreateProp { blueprint: \"abcd\", position: Position { x: 0, y: 10, z: -100 } }"
        );
        assert_eq!(
            format!(
                "{}",
                ExecuteLuaInSim {
                    code: "CallSomething(10 + 100)".into()
                }
            ),
            "ExecuteLuaInSim { code: \"CallSomething(10 + 100)\" }"
        );
        assert_eq!(
            format!("{}", SetCommandType { id: 1234, type_: 4 }),
            "SetCommandType { id: 1234, type: FormMove }"
        );
        assert_eq!(
            format!(
                "{}",
                SetCommandType {
                    id: 1234,
                    type_: 99
                }
            ),
            "SetCommandType { id: 1234, type: 99 }"
        );
        assert_eq!(
            format!(
                "{}",
                SetCommandTarget {
                    id: 1234,
                    target: Target::None
                }
            ),
            "SetCommandTarget { id: 1234, target: None }"
        );
        assert_eq!(
            format!(
                "{}",
                SetCommandTarget {
                    id: 1234,
                    target: Target::Entity { id: 1234 }
                }
            ),
            "SetCommandTarget { id: 1234, target: Entity { id: 1234 } }"
        );
        assert_eq!(
            format!(
                "{}",
                SetCommandTarget {
                    id: 1234,
                    target: Target::Position(Position {
                        x: 1.,
                        y: 2.,
                        z: 3.
                    })
                }
            ),
            "SetCommandTarget { id: 1234, target: Position { x: 1, y: 2, z: 3 } }"
        );

        let mut upgrades_table = LuaTable::new();
        upgrades_table.insert(
            LuaObject::Unicode("Name".into()),
            LuaObject::Unicode("Mazor".into()),
        );
        assert_eq!(
            format!("{}", IssueCommand(GameCommand {
                entity_ids: vec![1, 2],
                id: 12345,
                coordinated_attack_cmd_id: 0,
                type_: 0,
                arg2: -1,
                target: Target::Position(Position {
                    x: 1.,
                    y: 2.,
                    z: 3.
                }),
                arg3: 0,
                formation: Some(Formation {
                    a: 1.,
                    b: 2.,
                    c: 3.,
                    d: 4.,
                    scale: 2.,
                }),
                blueprint: "abcd".into(),
                arg4: 0,
                arg5: 1,
                arg6: 2,
                upgrades: LuaObject::Table(upgrades_table),
                clear_queue: None,
            })),
            "IssueCommand(GameCommand { entity_ids: [1, 2], id: 12345, coordinated_attack_cmd_id: 0, type: None, arg2: -1, target: Position { x: 1, y: 2, z: 3 }, arg3: 0, formation: Formation { a: 1, b: 2, c: 3, d: 4, scale: 2 }, blueprint: \"abcd\", arg4: 0, arg5: 1, arg6: 2, upgrades: {\"Name\": \"Mazor\"}, clear_queue: None })"
        );
    }
}
