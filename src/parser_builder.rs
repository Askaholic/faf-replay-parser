use crate::parser::{Parser, ParserOptions, StreamParser};
use crate::version::{CommandId, Version};
use std::collections::HashSet;

/// A builder for configuring replay [`Parser`]s. See [`build`] for an example.
///
/// [`build`]: struct.ParserBuilder.html#method.build
#[derive(Debug)]
pub struct ParserBuilder<V: Version> {
    options: ParserOptions<V>,
}

impl<V: Version> ParserBuilder<V> {
    /// Initialize with default options
    pub fn new() -> ParserBuilder<V> {
        ParserBuilder {
            options: ParserOptions {
                commands: HashSet::new(),
                limit: None,
                save_commands: true,
                stop_on_desync: true,
            },
        }
    }

    /// Creates a new [`Parser`] by moving the internal [`ParserOptions`] and consuming self.
    ///
    /// # Example
    ///
    /// ```rust
    /// use faf_replay_parser::scfa::{replay_command, ReplayCommandId};
    /// use faf_replay_parser::{ParserBuilder, SCFA};
    /// use std::convert::TryFrom;
    ///
    /// let parser = ParserBuilder::<SCFA>::new()
    ///     .commands(&[
    ///         ReplayCommandId::try_from(replay_command::ADVANCE).unwrap(),
    ///         ReplayCommandId::try_from(replay_command::VERIFY_CHECKSUM).unwrap(),
    ///         ReplayCommandId::try_from(replay_command::END_GAME).unwrap(),
    ///     ])
    ///     .build();
    /// ```
    pub fn build(self) -> Parser<V> {
        Parser::with_options(self.options)
    }

    /// Like [`build`] but creates a new [`StreamParser`].
    ///
    /// [`build`]: struct.ParserBuilder.html#method.build
    pub fn build_stream(self) -> StreamParser<V> {
        StreamParser::with_options(self.options)
    }

    /// Creates a new [`Parser`] by cloning the internal [`ParserOptions`]. Useful when constructing
    /// many parsers with similar options.
    ///
    /// Example
    ///
    /// ```rust
    /// use faf_replay_parser::scfa::{replay_command, ReplayCommandId};
    /// use faf_replay_parser::{ParserBuilder, SCFA};
    /// use std::convert::TryFrom;
    ///
    /// let mut builder = ParserBuilder::<SCFA>::new().commands(&[
    ///     ReplayCommandId::try_from(replay_command::ADVANCE).unwrap(),
    ///     ReplayCommandId::try_from(replay_command::END_GAME).unwrap(),
    /// ]);
    ///
    /// // Can only count game ticks
    /// let tick_parser = builder.build_clone();
    ///
    /// let builder =
    ///     builder.command(ReplayCommandId::try_from(replay_command::VERIFY_CHECKSUM).unwrap());
    /// // Can count game ticks and detect desyncs
    /// let desync_parser = builder.build_clone();
    ///
    /// let builder =
    ///     builder.command(ReplayCommandId::try_from(replay_command::LUA_SIM_CALLBACK).unwrap());
    /// // Can count game ticks, detect desyncs, and extract chat messages
    /// let message_parser = builder.build();
    /// ```
    pub fn build_clone(&self) -> Parser<V> {
        Parser::with_options(self.options.clone())
    }

    /// Like [`build_clone`] but creates a new [`StreamParser`].
    ///
    /// [`build_clone`]: struct.ParserBuilder.html#method.build_clone
    pub fn build_stream_clone(&self) -> StreamParser<V> {
        StreamParser::with_options(self.options.clone())
    }

    /// Add a command to the [`ParserOptions`].
    pub fn command(mut self, command: V::CommandId) -> ParserBuilder<V> {
        self.options.commands.insert(command);
        self
    }

    /// Add multiple commands to the [`ParserOptions`].
    pub fn commands(mut self, commands: &[V::CommandId]) -> ParserBuilder<V> {
        commands.iter().for_each(|cmd| {
            self.options.commands.insert(*cmd);
        });
        self
    }

    /// Add all supported commands to the [`ParserOptions`].
    pub fn commands_all(self) -> ParserBuilder<V> {
        self.commands(
            V::CommandId::builder_all()
                .collect::<Vec<V::CommandId>>()
                .as_slice(),
        )
    }

    /// Add the default set of commands to the [`ParserOptions`].
    ///
    /// Default commands are `ADVANCE`, `SET_COMMAND_SOURCE`, `COMMAND_SOURCE_TERMINATED`,
    /// `VERIFY_CHECKSUM`, and `END_GAME`
    pub fn commands_default(self) -> ParserBuilder<V> {
        self.commands(
            V::CommandId::builder_defaults()
                .collect::<Vec<V::CommandId>>()
                .as_slice(),
        )
    }

    /// Set a limit on the number of commands to parse. This only applies to commands in the
    /// `ParserOptions.commands` field.
    pub fn limit(mut self, limit: Option<usize>) -> ParserBuilder<V> {
        self.options.limit = limit;
        self
    }

    /// Whether or not to store the parsed commands in the parsed `ReplayBody`. When set to false,
    /// `Replay.body.commands.len()` will always be `0`.
    pub fn save_commands(mut self, save_commands: bool) -> ParserBuilder<V> {
        self.options.save_commands = save_commands;
        self
    }

    /// Whether or not to return an error if a desync is detected. When set to false,
    /// `Replay.sim.desync_ticks` will be a list of all desynced ticks.
    pub fn stop_on_desync(mut self, stop_on_desync: bool) -> ParserBuilder<V> {
        self.options.stop_on_desync = stop_on_desync;
        self
    }
}
