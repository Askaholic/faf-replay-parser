//! Struct definitions for Supreme Commander: Forged Alliance replays

use faf_replay_parser_derive::Display;
use std::convert::TryFrom;
use std::fmt::Debug;

use crate::display::{HexArray, TransparentOption};
use crate::lua::LuaObject;
use crate::reader::ReplayReadError;
use crate::version::CommandId;

// TODO: Rename to SCFAReplayCommand or SupremeCommanderForgedAllianceReplayCommand?
#[derive(Clone, Display, Debug, PartialEq)]
pub enum ReplayCommand {
    /// Advance for some number of ticks
    Advance {
        ticks: u32,
    },
    SetCommandSource {
        id: u8,
    },
    CommandSourceTerminated,
    /// Verify that all command streams agree
    ///
    /// If one of the command streams does not agree, then the game has desynced.
    VerifyChecksum {
        #[display(fmt_with = display_digest)]
        digest: [u8; 16],
        tick: u32,
    },
    RequestPause,
    Resume,
    SingleStep,
    /// u8 - Owner army
    /// string - Blueprint identifier
    CreateUnit {
        army: u8,
        #[display(debug)]
        blueprint: String,
        x: f32,
        z: f32,
        heading: f32,
    },
    CreateProp {
        #[display(debug)]
        blueprint: String,
        position: Position,
    },
    DestroyEntity {
        unit: u32,
    },
    WarpEntity {
        unit: u32,
        x: f32,
        y: f32,
        z: f32,
    },
    /// Set certain unit attributes such as their name or whether or not they are paused
    ProcessInfoPair {
        unit: u32,
        #[display(debug)]
        arg1: String,
        #[display(debug)]
        arg2: String,
    },
    /// Add a command to a unit's command queue
    IssueCommand(GameCommand),
    /// Add a command to a factory's command queue
    IssueFactoryCommand(GameCommand),
    IncreaseCommandCount {
        id: u32,
        delta: i32,
    },
    DecreaseCommandCount {
        id: u32,
        delta: i32,
    },
    /// CmdId - command id
    /// STITarget - target
    SetCommandTarget {
        id: u32,
        target: Target,
    },
    /// CmdId - command id
    /// EUnitCommandType - type
    SetCommandType {
        id: u32,
        #[display(name = "type", fmt_with = display_game_command_type)]
        type_: u8,
    },
    /// CmdId - command id
    /// ListOfCells - list of cells
    /// Vector3f - pos
    SetCommandCells {
        id: u32,
        cells: LuaObject,
        position: Position,
    },
    /// CmdId - command id
    /// EntId - unit
    RemoveCommandFromQueue {
        id: u32,
        unit: u32,
    },
    /// string -- the debug command string
    /// Vector3f -- mouse pos (in world coords)
    /// uint8 -- focus army index
    /// EntIdSet -- selection
    DebugCommand {
        #[display(debug)]
        command: String,
        position: Position,
        focus_army: u8,
        #[display(debug)]
        selection: Vec<u32>,
    },
    /// A lua string to evaluate in the simulation
    ExecuteLuaInSim {
        #[display(debug)]
        code: String,
    },
    // From SupremeCommander.exe strings:
    // SimCallback(callback[,bool]): Execute a lua function in sim
    // callback = {
    //    Func    =   function name (in the SimCallbacks.lua module) to call
    //    Args    =   Arguments as a lua object
    // If bool is specified and true, sends the current selection with the command
    LuaSimCallback {
        #[display(debug)]
        func: String,
        args: LuaObject,
        /// A list of entity id's that are currently selected
        #[display(debug)]
        selection: Vec<u32>,
    },
    EndGame,
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct ReplayCommandId(u8);

impl TryFrom<u8> for ReplayCommandId {
    type Error = ReplayReadError;

    fn try_from(val: u8) -> Result<Self, Self::Error> {
        if val > Self::max() {
            return Err(ReplayReadError::Malformed("invalid command"));
        }
        Ok(Self(val))
    }
}

impl CommandId for ReplayCommandId {
    fn advance() -> Self {
        Self(replay_command::ADVANCE)
    }

    fn as_u8(&self) -> u8 {
        self.0
    }

    fn builder_all() -> impl Iterator<Item = Self> {
        (0..=Self::max()).map(|c| Self(c))
    }

    fn builder_defaults() -> impl Iterator<Item = Self> {
        [
            replay_command::ADVANCE,
            replay_command::SET_COMMAND_SOURCE,
            replay_command::COMMAND_SOURCE_TERMINATED,
            replay_command::VERIFY_CHECKSUM,
            replay_command::END_GAME,
        ]
        .iter()
        .map(|c| Self(*c))
    }

    fn max() -> u8 {
        replay_command::MAX
    }
}

/// Command names for the replay body data
pub mod replay_command {
    pub const ADVANCE: u8 = 0;
    pub const SET_COMMAND_SOURCE: u8 = 1;
    pub const COMMAND_SOURCE_TERMINATED: u8 = 2;
    pub const VERIFY_CHECKSUM: u8 = 3;
    pub const REQUEST_PAUSE: u8 = 4;
    pub const RESUME: u8 = 5;
    pub const SINGLE_STEP: u8 = 6;
    pub const CREATE_UNIT: u8 = 7;
    pub const CREATE_PROP: u8 = 8;
    pub const DESTROY_ENTITY: u8 = 9;
    pub const WARP_ENTITY: u8 = 10;
    pub const PROCESS_INFO_PAIR: u8 = 11;
    pub const ISSUE_COMMAND: u8 = 12;
    pub const ISSUE_FACTORY_COMMAND: u8 = 13;
    pub const INCREASE_COMMAND_COUNT: u8 = 14;
    pub const DECREASE_COMMAND_COUNT: u8 = 15;
    pub const SET_COMMAND_TARGET: u8 = 16;
    pub const SET_COMMAND_TYPE: u8 = 17;
    pub const SET_COMMAND_CELLS: u8 = 18;
    pub const REMOVE_COMMAND_FROM_QUEUE: u8 = 19;
    pub const DEBUG_COMMAND: u8 = 20;
    pub const EXECUTE_LUA_IN_SIM: u8 = 21;
    pub const LUA_SIM_CALLBACK: u8 = 22;
    pub const END_GAME: u8 = 23;
    /// The maximum valid command type
    pub const MAX: u8 = END_GAME;

    pub static NAMES: [&str; MAX as usize + 1] = [
        "Advance",
        "SetCommandSource",
        "CommandSourceTerminated",
        "VerifyChecksum",
        "RequestPause",
        "Resume",
        "SingleStep",
        "CreateUnit",
        "CreateProp",
        "DestroyEntity",
        "WarpEntity",
        "ProcessInfoPair",
        "IssueCommand",
        "IssueFactoryCommand",
        "IncreaseCommandCount",
        "DecreaseCommandCount",
        "SetCommandTarget",
        "SetCommandType",
        "SetCommandCells",
        "RemoveCommandFromQueue",
        "DebugCommand",
        "ExecuteLuaInSim",
        "LuaSimCallback",
        "EndGame",
    ];
}

fn display_game_command_type(type_: &u8) -> std::borrow::Cow<'static, str> {
    game_command::NAMES
        .get(*type_ as usize)
        .map(|name| std::borrow::Cow::Borrowed(*name))
        .unwrap_or_else(|| std::borrow::Cow::Owned(format!("{}", type_)))
}

fn display_digest(digest: &[u8; 16]) -> HexArray<'_> {
    HexArray(&digest[..])
}

fn display_option<T: std::fmt::Display>(option: &Option<T>) -> TransparentOption<'_, T> {
    TransparentOption(option)
}

#[derive(Clone, Display, Debug, PartialEq)]
pub struct Position {
    /// East/West direction
    pub x: f32,
    /// Vertical direction
    pub y: f32,
    /// North/South direction
    pub z: f32,
}

pub mod target_type {
    pub const NONE: u8 = 0;
    pub const ENTITY: u8 = 1;
    pub const POSITION: u8 = 2;
}

#[derive(Clone, Display, Debug, PartialEq)]
pub enum Target {
    None,
    Entity {
        id: u32,
    },
    /// Where the top left corner of the map is (0, 0, 0)
    #[display(transparent)]
    Position(Position),
}

/// Quaternion representing orientation and a scale value
#[derive(Clone, Display, Debug, PartialEq)]
pub struct Formation {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
    pub scale: f32,
}

#[derive(Clone, Display, Debug, PartialEq)]
pub struct Cell {
    pub x: i16,
    pub z: i16,
}

/// The data contained by `IssueCommand` and `IssueFactoryCommand`. Some of the fields are
/// unknown as the documentation that used to exist on the gpg forums has been lost.
#[derive(Clone, Display, Debug, PartialEq)]
pub struct GameCommand {
    #[display(debug)]
    pub entity_ids: Vec<u32>,
    pub id: u32,
    /// References another command in the command queue. This is known to reference another attack
    /// command in the case of a "coordinated attack", but it may be used for other cases too.
    pub coordinated_attack_cmd_id: i32,
    #[display(name = "type", fmt_with = display_game_command_type)]
    pub type_: u8,
    pub arg2: i32,
    pub target: Target,
    pub arg3: u8,
    #[display(fmt_with = display_option)]
    pub formation: Option<Formation>,
    #[display(debug)]
    pub blueprint: String,
    pub arg4: u32,
    pub arg5: u32,
    pub arg6: u32,
    /// A Lua table for selecting commander upgrades. The table always consists of two keys:
    ///
    /// ```json
    /// {
    ///     "TaskName": "EnhanceTask",
    ///     "Enhancement": "AdvancedEngineering"
    /// }
    /// ```
    pub upgrades: LuaObject,
    /// Indicates whether or not starting this upgrade clears the current command queue.
    /// Only appears if `upgrades` is non empty.
    #[display(fmt_with = display_option)]
    pub clear_queue: Option<bool>,
}

/// Constants refering to the `EUnitCommandType` enumeration
pub mod game_command {
    pub const NONE: u8 = 0;
    pub const STOP: u8 = 1;
    pub const MOVE: u8 = 2;
    pub const DIVE: u8 = 3;
    pub const FORM_MOVE: u8 = 4;
    pub const BUILD_SILO_TACTICAL: u8 = 5;
    pub const BUILD_SILO_NUKE: u8 = 6;
    pub const BUILD_FACTORY: u8 = 7;
    pub const BUILD_MOBILE: u8 = 8;
    pub const BUILD_ASSIST: u8 = 9;
    pub const ATTACK: u8 = 10;
    pub const FORM_ATTACK: u8 = 11;
    pub const NUKE: u8 = 12;
    pub const TACTICAL: u8 = 13;
    pub const TELEPORT: u8 = 14;
    pub const GUARD: u8 = 15;
    pub const PATROL: u8 = 16;
    pub const FERRY: u8 = 17;
    pub const FORM_PATROL: u8 = 18;
    pub const RECLAIM: u8 = 19;
    pub const REPAIR: u8 = 20;
    pub const CAPTURE: u8 = 21;
    pub const TRANSPORT_LOAD_UNITS: u8 = 22;
    pub const TRANSPORT_REVERSE_LOAD_UNITS: u8 = 23;
    pub const TRANSPORT_UNLOAD_UNITS: u8 = 24;
    pub const TRANSPORT_UNLOAD_SPECIFIC_UNITS: u8 = 25;
    pub const DETACH_FROM_TRANSPORT: u8 = 26;
    pub const UPGRADE: u8 = 27;
    pub const SCRIPT: u8 = 28;
    pub const ASSIST_COMMANDER: u8 = 29;
    pub const KILL_SELF: u8 = 30;
    pub const DESTROY_SELF: u8 = 31;
    pub const SACRIFICE: u8 = 32;
    pub const PAUSE: u8 = 33;
    pub const OVER_CHARGE: u8 = 34;
    pub const AGGRESSIVE_MOVE: u8 = 35;
    pub const FORM_AGGRESSIVE_MOVE: u8 = 36;
    pub const ASSIST_MOVE: u8 = 37;
    pub const SPECIAL_ACTION: u8 = 38;
    pub const DOCK: u8 = 39;
    /// The maximum valid command type
    pub const MAX: u8 = 39;

    pub static NAMES: [&str; MAX as usize + 1] = [
        "None",
        "Stop",
        "Move",
        "Dive",
        "FormMove",
        "BuildSiloTactical",
        "BuildSiloNuke",
        "BuildFactory",
        "BuildMobile",
        "BuildAssist",
        "Attack",
        "FormAttack",
        "Nuke",
        "Tactical",
        "Teleport",
        "Guard",
        "Patrol",
        "Ferry",
        "FormPatrol",
        "Reclaim",
        "Repair",
        "Capture",
        "TransportLoadUnits",
        "TransportReverseLoadUnits",
        "TransportUnloadUnits",
        "TransportUnloadSpecificUnits",
        "DetachFromTransport",
        "Upgrade",
        "Script",
        "AssistCommander",
        "KillSelf",
        "DestroySelf",
        "Sacrifice",
        "Pause",
        "OverCharge",
        "AggressiveMove",
        "FormAggressiveMove",
        "AssistMove",
        "SpecialAction",
        "Dock",
    ];
}
