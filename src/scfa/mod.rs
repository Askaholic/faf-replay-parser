//! Supreme Commander: Forged Alliance data format

pub mod bytes;
pub mod parser;
pub mod replay;

pub use self::bytes::{body_offset, body_ticks, has_frame};
pub use replay::*;

use crate::version::Version;

/// Replay version for Supreme Commander: Forged Alliance replays. May also work with vanilla
/// Supreme Commander replays as well.
#[derive(Debug)]
pub enum SCFA {}

impl Version for SCFA {
    type Command = ReplayCommand;
    type CommandId = ReplayCommandId;
}
