//! Trait definitions for parsing different replay versions
//!
//! The replay formats for the family of Supreme Commander games are all very similar to each
//! other. They use the same command framing but not necessarily the same set of valid commands
//! or same mapping of command ids. The traits in this module are used to abstract over these
//! differences. Users of this library only need an implementor of the [`Version`] trait. This
//! trait can be used to instruct the parsing functions which Supreme Commander format to follow.

use crate::reader::ReplayResult;
use std::convert::TryFrom;
use std::fmt::{Debug, Display};
use std::hash::Hash;

use crate::replay::SimData;

/// The top level trait for defining how to parse replays of a certain game version. See the
/// [Implementors](trait.Version.html#implementors) for a list of implementations that can be used
/// with the parsing functions.
///
/// For implementors it is recommend to implement this trait on an uninstantiable object.
///
/// ```rust
/// #[derive(Debug)]
/// pub enum CustomVersion {}
/// ```
pub trait Version: Debug {
    /// An enum that commands will be parsed into.
    type Command: Command;
    /// A newtype integer for valid command ids for this version. This is used to validate command
    /// ids before parsing the command data.
    type CommandId: CommandId;
}

/// A trait that must be implemented for the enum that command data will be parsed into.
pub trait Command: Sized + Debug + Display + Clone + PartialEq {
    /// Attempt to cast as an [`Advance`] command.
    fn as_advance(&self) -> Option<Advance>;

    /// Parse into a `Command` with given command id and buffer.
    fn parse_command_data(command_id: u8, reader: &[u8]) -> ReplayResult<Self>;

    /// Update `sim` given the new command.
    fn process_command(sim: &mut SimData, command: &Self) -> ReplayResult<()>;
}

/// A trait that must be implemented on a newtype u8, representing command ids for a certain
/// version of Supreme Commander.
///
/// The following example shows the recommended way to implement required trait bounds. Note that
/// [`TryFrom::try_from`] will be used to perform id validation, so it is important to implement it
/// and only allow conversion from `u8` if the value maps to a valid command id.
///
/// ```rust
/// use faf_replay_parser::ReplayReadError;
/// use faf_replay_parser::version::CommandId;
/// use std::convert::TryFrom;
///
/// #[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
/// pub struct CustomCommandId(u8);
///
/// impl TryFrom<u8> for CustomCommandId {
///     type Error = ReplayReadError;
///
///     fn try_from(val: u8) -> Result<Self, Self::Error> {
///         if val > Self::max() {
///             return Err(ReplayReadError::Malformed("invalid command"));
///         }
///         Ok(Self(val))
///     }
/// }
///
/// impl CommandId for CustomCommandId {
///     fn advance() -> Self { Self(0) }
///     fn as_u8(&self) -> u8 { self.0 }
///     fn builder_all() -> impl Iterator<Item = Self> {
///         (0..=Self::max()).map(|c| Self(c))
///     }
///     fn builder_defaults() -> impl Iterator<Item = Self> {
///         [0, 1, 2].iter().map(|c| Self(*c))
///     }
///     fn max() -> u8 { 5 }
/// }
/// ```
pub trait CommandId: Sized + Debug + Clone + Copy + Eq + Hash + TryFrom<u8> {
    /// Returns an instance of `Self` that represents the `Advance` command.
    ///
    /// `Advance` is an essential command telling the simulation to advance for a certain number of
    /// ticks. This is used to determine the in-game time stamps of other commands and so almost
    /// always needs to be parsed.
    fn advance() -> Self;

    /// Returns the byte value of this command id.
    fn as_u8(&self) -> u8;

    /// Returns an iterator that is used to initialize a [`Parser`](struct.Parser.html) when
    /// [`ParserBuilder::commands_all`](struct.ParserBuilder.html#method.commands_all) is called.
    fn builder_all() -> impl Iterator<Item = Self>;

    /// Returns an iterator that is used to initialize a [`Parser`](struct.Parser.html) when
    /// [`ParserBuilder::commands_default`](struct.ParserBuilder.html#method.commands_default) is
    /// called.
    fn builder_defaults() -> impl Iterator<Item = Self>;

    /// Returns the maximum valid byte value for a command id.
    fn max() -> u8;
}

pub struct Advance {
    pub ticks: u32,
}
