pub mod parser;
pub mod replay;

pub use replay::*;

use crate::version::Version;

/// Replay version for Supreme Commander 2.
#[derive(Debug)]
pub enum SC2 {}

impl Version for SC2 {
    type Command = ReplayCommand;
    type CommandId = ReplayCommandId;
}
