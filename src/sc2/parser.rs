use super::replay::*;
use crate::reader::{ReplayBufReadExt, ReplayReadError, ReplayReadExt, ReplayResult};
use crate::replay::SimData;
use crate::version::Command;
use std::io::{BufRead, Read};

impl Command for ReplayCommand {
    fn as_advance(&self) -> Option<crate::version::Advance> {
        match self {
            Self::Advance { ticks } => Some(crate::version::Advance { ticks: *ticks }),
            _ => None,
        }
    }

    fn parse_command_data(command_id: u8, mut reader: &[u8]) -> ReplayResult<ReplayCommand> {
        use replay_command::*;
        use ReplayCommand::*;

        let cmd = match command_id {
            ADVANCE => Advance {
                ticks: reader.read_u32_le()?,
            },
            SET_COMMAND_SOURCE => SetCommandSource {
                id: reader.read_u8()?,
            },
            COMMAND_SOURCE_TERMINATED => CommandSourceTerminated,
            VERIFY_CHECKSUM => {
                let mut digest = [0; 16];
                reader.read_exact(&mut digest)?;
                VerifyChecksum {
                    digest,
                    tick: reader.read_u32_le()?,
                }
            }
            REQUEST_PAUSE => RequestPause,
            RESUME => Resume,
            SINGLE_STEP => SingleStep,
            CREATE_UNIT => CreateUnit {
                army: reader.read_u8()?,
                blueprint: reader.read_string()?,
                x: reader.read_f32_le()?,
                z: reader.read_f32_le()?,
                heading: reader.read_f32_le()?,
            },
            CREATE_PROP => CreateProp {
                blueprint: reader.read_string()?,
                position: read_position(&mut reader)?,
            },
            DESTROY_ENTITY => DestroyEntity {
                unit: reader.read_u32_le()?,
            },
            WARP_ENTITY => WarpEntity {
                unit: reader.read_u32_le()?,
                x: reader.read_f32_le()?,
                y: reader.read_f32_le()?,
                z: reader.read_f32_le()?,
            },
            PROCESS_INFO_PAIR => ProcessInfoPair {
                unit: reader.read_u32_le()?,
                arg1: reader.read_string()?,
                arg2: reader.read_string()?,
            },
            ISSUE_COMMAND => IssueCommand(read_game_command(&mut reader)?),
            COMMAND_16 => Command16(read_game_command(&mut reader)?),
            ISSUE_FACTORY_COMMAND => IssueFactoryCommand(read_game_command(&mut reader)?),
            COMMAND_18 => Command18 {
                id: reader.read_u32_le()?,
                source_id: reader.read_u8()?,
                unknown: reader.vec_read_to_end()?,
            },
            INCREASE_COMMAND_COUNT => IncreaseCommandCount {
                id: reader.read_u32_le()?,
                delta: reader.read_i32_le()?,
            },
            DECREASE_COMMAND_COUNT => DecreaseCommandCount {
                id: reader.read_u32_le()?,
                delta: reader.read_i32_le()?,
            },
            SET_COMMAND_TARGET => SetCommandTarget {
                id: reader.read_u32_le()?,
                target: read_target(&mut reader)?,
            },
            SET_COMMAND_TYPE => {
                let id = reader.read_u32_le()?;
                let type_ = reader.read_u32_le()? as u8;
                if type_ > game_command::MAX {
                    return Err(ReplayReadError::Malformed("invalid game command"));
                }
                SetCommandType { id, type_ }
            }
            SET_COMMAND_CELLS => {
                // This function may or may not be correct. So far, we have not found any replays that
                // contain this command.
                let id = reader.read_u32_le()?;
                let cells = reader.read_lua_object()?;
                if cells.evaluate_as_bool() {
                    reader.skip_buf(1, &mut [0])?;
                }
                SetCommandCells {
                    id,
                    cells,
                    position: read_position(&mut reader)?,
                }
            }
            REMOVE_COMMAND_FROM_QUEUE => RemoveCommandFromQueue {
                id: reader.read_u32_le()?,
                unit: reader.read_u32_le()?,
            },
            DEBUG_COMMAND => DebugCommand {
                command: reader.read_string()?,
                position: read_position(&mut reader)?,
                focus_army: reader.read_u8()?,
                selection: read_entity_list(&mut reader)?,
            },
            EXECUTE_LUA_IN_SIM => ExecuteLuaInSim {
                code: reader.read_string()?,
            },
            LUA_SIM_CALLBACK => {
                let func = reader.read_string()?;
                let args = reader.read_lua_object()?;

                let selection = read_entity_list(&mut reader)?;
                LuaSimCallback {
                    func,
                    args,
                    selection,
                }
            }
            END_GAME => EndGame,
            id => Unknown {
                cmd: id,
                data: reader.vec_read_to_end()?,
            },
        };
        Ok(cmd)
    }

    /// Update `sim` given the new command.
    fn process_command(sim: &mut SimData, command: &ReplayCommand) -> ReplayResult<()> {
        use ReplayCommand::*;

        match command {
            // Increment the tick counter
            Advance { ticks } => sim.tick += *ticks,
            // Change which player the following commands apply to
            SetCommandSource { id } => sim.command_source = *id,
            // Record when command a command stream ends
            CommandSourceTerminated => {
                sim.players_last_tick.insert(sim.command_source, sim.tick);
            }
            // Make sure that the replay hasn't desynced. Every player sends a checksum once per 50
            // ticks representing the game state of their local simulation. If one or more of the
            // checksums don't match, then those players have desynced and we can no longer trust their
            // replay streams.
            VerifyChecksum { digest, tick } => {
                if sim.checksum_tick < Some(*tick) {
                    // First checksum submitted for this tick
                    sim.checksum_tick.replace(*tick);
                    sim.checksum.copy_from_slice(&digest[..16]);
                    return Ok(());
                }
                debug_assert_eq!(
                    sim.checksum_tick,
                    Some(*tick),
                    "Checksum received out of order!"
                );

                if sim.checksum != digest[..16] {
                    if sim.desync_tick.is_none() {
                        sim.desync_tick.replace(sim.tick);
                        sim.desync_ticks.replace(vec![]);
                    }

                    sim.desync_ticks.as_mut().unwrap().push(sim.tick);

                    // TODO: Use a different error type here? ParseError?
                    return Err(ReplayReadError::Desynced(sim.tick));
                }
            }
            _ => (),
        };
        Ok(())
    }
}

#[inline]
pub fn read_position(reader: &mut impl Read) -> ReplayResult<Position> {
    Ok(Position {
        x: reader.read_f32_le()?,
        y: reader.read_f32_le()?,
        z: reader.read_f32_le()?,
    })
}

pub fn read_entity_list(reader: &mut impl Read) -> ReplayResult<Vec<u32>> {
    let len = reader.read_u32_le()? as usize;
    // 16 players at max unit cap selecting every entity at once.
    // This should be way more than enough.
    let mut result = Vec::with_capacity(std::cmp::min(len, 16_000));

    for _ in 0..len {
        result.push(reader.read_u32_le()?);
    }

    Ok(result)
}

pub fn read_target(reader: &mut impl Read) -> ReplayResult<Target> {
    use target_type::*;

    match reader.read_u8()? {
        NONE => Ok(Target::None),
        ENTITY => Ok(Target::Entity {
            id: reader.read_u32_le()?,
        }),
        POSITION => Ok(Target::Position(read_position(reader)?)),
        _ => Err(ReplayReadError::Malformed("invalid target type")),
    }
}

pub fn read_formation(reader: &mut impl Read) -> ReplayResult<Option<Formation>> {
    let type_ = reader.read_i32_le()?;
    if type_ == -1 {
        return Ok(None);
    }

    Ok(Some(Formation {
        a: reader.read_f32_le()?,
        b: reader.read_f32_le()?,
        c: reader.read_f32_le()?,
        d: reader.read_f32_le()?,
        scale: reader.read_f32_le()?,
    }))
}

pub fn read_cells(reader: &mut impl Read) -> ReplayResult<Vec<Cell>> {
    let size = reader.read_u32_le()? as usize;
    // Since we've never seen these, lets just limit to 1 MiB
    let mut result = Vec::with_capacity(std::cmp::min(
        size,
        crate::MIB / std::mem::size_of::<Cell>(),
    ));
    for _ in 0..size {
        result.push(Cell {
            x: reader.read_i16_le()?,
            z: reader.read_i16_le()?,
        });
    }
    Ok(result)
}

pub fn read_game_command(reader: &mut (impl Read + BufRead)) -> ReplayResult<GameCommand> {
    let entity_ids = read_entity_list(reader)?;
    let id = reader.read_u32_le()?;

    // Guessing that 0xFFFFFFFF for this field means it's a signed int
    // Appears to be another command id referencing the target of a coordinated attack.
    // When this is not -1, type_ is always 'ATTACK (10)'
    let coordinated_attack_cmd_id = reader.read_i32_le()?;
    let type_ = reader.read_u8()?;

    if type_ > game_command::MAX {
        return Err(ReplayReadError::Malformed("invalid game command"));
    }
    // Appears to be a command id or entity id
    // When this is not -1, type_ is always "BUILD_FACTORY (10)"
    let arg2 = reader.read_i32_le()?;

    let target = read_target(reader)?;
    let arg3 = reader.read_u8()?; // Always 0
    let formation = read_formation(reader)?;

    let blueprint = reader.by_ref().take(255).read_string()?;

    // TODO: Are these signed or unsigned? What do they mean?
    let arg4 = reader.read_u32_le()?; // Always 0
    let arg5 = reader.read_u32_le()?; // Always 1
    let arg6 = reader.read_u32_le()?; // Always 1

    let mut unknown = [0; 8];
    reader.read_exact(&mut unknown)?;

    let mut clear_queue = None;

    let ability = reader.read_lua_object()?;
    if ability.evaluate_as_bool() {
        clear_queue = Some(reader.read_bool()?);
    }

    let unknown2 = reader.vec_read_to_end()?;

    Ok(GameCommand {
        entity_ids,
        id,
        coordinated_attack_cmd_id,
        type_,
        arg2,
        target,
        arg3,
        formation,
        blueprint,
        arg4,
        arg5,
        arg6,
        unknown,
        ability,
        clear_queue,
        unknown2,
    })
}
