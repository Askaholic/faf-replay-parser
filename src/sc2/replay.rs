//! Struct definitions for Supreme Commander 2 replays

use faf_replay_parser_derive::Display;
use std::convert::TryFrom;
use std::fmt::Debug;

use crate::display::{HexArray, TransparentOption};
use crate::lua::LuaObject;
use crate::reader::ReplayReadError;
use crate::version::CommandId;

// TODO: Rename to SC2ReplayCommand or SupremeCommander2ReplayCommand?
#[derive(Clone, Display, Debug, PartialEq)]
pub enum ReplayCommand {
    /// Advance for some number of ticks
    Advance {
        ticks: u32,
    },
    SetCommandSource {
        id: u8,
    },
    CommandSourceTerminated,
    /// Verify that all command streams agree
    ///
    /// If one of the command streams does not agree, then the game has desynced.
    VerifyChecksum {
        #[display(fmt_with = display_digest)]
        digest: [u8; 16],
        tick: u32,
    },
    RequestPause,
    Resume,
    SingleStep,
    /// u8 - Owner army
    /// string - Blueprint identifier
    CreateUnit {
        army: u8,
        #[display(debug)]
        blueprint: String,
        x: f32,
        z: f32,
        heading: f32,
    },
    CreateProp {
        #[display(debug)]
        blueprint: String,
        position: Position,
    },
    DestroyEntity {
        unit: u32,
    },
    WarpEntity {
        unit: u32,
        x: f32,
        y: f32,
        z: f32,
    },
    /// Set certain unit attributes such as their name or whether or not they are paused
    ProcessInfoPair {
        unit: u32,
        #[display(debug)]
        arg1: String,
        #[display(debug)]
        arg2: String,
    },
    /// Add a command to a unit's command queue
    IssueCommand(GameCommand),
    // Seems to alwasy be a BuildFactory command. Maybe for an experimental gantry?
    Command16(GameCommand),
    /// Add a command to a factory's command queue
    IssueFactoryCommand(GameCommand),
    Command18 {
        /// Command ID, but not referencing an existing command
        id: u32,
        // Appears to always match the id from previous SetCommandSource.
        source_id: u8,
        #[display(fmt_with = display_digest)]
        unknown: Vec<u8>,
    },
    IncreaseCommandCount {
        id: u32,
        delta: i32,
    },
    DecreaseCommandCount {
        id: u32,
        delta: i32,
    },
    /// CmdId - command id
    /// STITarget - target
    SetCommandTarget {
        id: u32,
        target: Target,
    },
    /// CmdId - command id
    /// EUnitCommandType - type
    SetCommandType {
        id: u32,
        #[display(name = "type", fmt_with = display_game_command_type)]
        type_: u8,
    },
    /// CmdId - command id
    /// ListOfCells - list of cells
    /// Vector3f - pos
    SetCommandCells {
        id: u32,
        cells: LuaObject,
        position: Position,
    },
    /// CmdId - command id
    /// EntId - unit
    RemoveCommandFromQueue {
        id: u32,
        unit: u32,
    },
    /// string -- the debug command string
    /// Vector3f -- mouse pos (in world coords)
    /// uint8 -- focus army index
    /// EntIdSet -- selection
    DebugCommand {
        #[display(debug)]
        command: String,
        position: Position,
        focus_army: u8,
        #[display(debug)]
        selection: Vec<u32>,
    },
    /// A lua string to evaluate in the simulation
    ExecuteLuaInSim {
        #[display(debug)]
        code: String,
    },
    // From SupremeCommander.exe strings:
    // SimCallback(callback[,bool]): Execute a lua function in sim
    // callback = {
    //    Func    =   function name (in the SimCallbacks.lua module) to call
    //    Args    =   Arguments as a lua object
    // If bool is specified and true, sends the current selection with the command
    LuaSimCallback {
        #[display(debug)]
        func: String,
        args: LuaObject,
        /// A list of entity id's that are currently selected
        #[display(debug)]
        selection: Vec<u32>,
    },
    EndGame,
    Unknown {
        cmd: u8,
        #[display(fmt_with = display_digest)]
        data: Vec<u8>,
    },
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct ReplayCommandId(u8);

impl TryFrom<u8> for ReplayCommandId {
    type Error = ReplayReadError;

    fn try_from(val: u8) -> Result<Self, Self::Error> {
        if val > Self::max() {
            return Err(ReplayReadError::Malformed("invalid command"));
        }
        Ok(Self(val))
    }
}

impl CommandId for ReplayCommandId {
    fn advance() -> Self {
        Self(replay_command::ADVANCE)
    }

    fn as_u8(&self) -> u8 {
        self.0
    }

    fn builder_all() -> impl Iterator<Item = Self> {
        (0..=Self::max()).map(|c| Self(c))
    }

    fn builder_defaults() -> impl Iterator<Item = Self> {
        [
            replay_command::ADVANCE,
            replay_command::SET_COMMAND_SOURCE,
            replay_command::COMMAND_SOURCE_TERMINATED,
            replay_command::VERIFY_CHECKSUM,
            replay_command::END_GAME,
        ]
        .iter()
        .map(|c| Self(*c))
    }

    fn max() -> u8 {
        replay_command::MAX
    }
}

/// Command names for the replay body data
pub mod replay_command {
    pub const ADVANCE: u8 = 0;
    pub const SET_COMMAND_SOURCE: u8 = 1;
    pub const COMMAND_SOURCE_TERMINATED: u8 = 2;
    pub const VERIFY_CHECKSUM: u8 = 3;
    pub const REQUEST_PAUSE: u8 = 4;
    pub const RESUME: u8 = 5;
    pub const SINGLE_STEP: u8 = 6;
    pub const CREATE_UNIT: u8 = 7;
    pub const CREATE_PROP: u8 = 8;
    pub const DESTROY_ENTITY: u8 = 9;
    pub const WARP_ENTITY: u8 = 10;
    pub const COMMAND_11: u8 = 11;
    pub const COMMAND_12: u8 = 12;
    pub const COMMAND_13: u8 = 13;
    // Different from SCFA
    // Confirmed
    pub const PROCESS_INFO_PAIR: u8 = 14;
    // Confirmed
    pub const ISSUE_COMMAND: u8 = 15;
    pub const COMMAND_16: u8 = 16;
    // Pretty sure
    pub const ISSUE_FACTORY_COMMAND: u8 = 17;
    pub const COMMAND_18: u8 = 18;
    pub const COMMAND_19: u8 = 19;
    // Pretty sure
    pub const INCREASE_COMMAND_COUNT: u8 = 20;
    // Pretty sure
    pub const DECREASE_COMMAND_COUNT: u8 = 21;
    pub const SET_COMMAND_TARGET: u8 = 22;
    pub const SET_COMMAND_TYPE: u8 = 23;
    pub const SET_COMMAND_CELLS: u8 = 24;
    pub const REMOVE_COMMAND_FROM_QUEUE: u8 = 25;
    pub const DEBUG_COMMAND: u8 = 26;
    pub const EXECUTE_LUA_IN_SIM: u8 = 27;
    // Confirmed
    pub const LUA_SIM_CALLBACK: u8 = 28;
    pub const END_GAME: u8 = 29;

    /// The maximum valid command type
    pub const MAX: u8 = END_GAME;

    pub static NAMES: [&str; MAX as usize + 1] = [
        "Advance",
        "SetCommandSource",
        "CommandSourceTerminated",
        "VerifyChecksum",
        "RequestPause",
        "Resume",
        "SingleStep",
        "CreateUnit",
        "CreateProp",
        "DestroyEntity",
        "WarpEntity",
        "Command11",
        "Command12",
        "Command13",
        "ProcessInfoPair",
        "IssueCommand",
        "Command16",
        "IssueFactoryCommand",
        "Command18",
        "Command19",
        "IncreaseCommandCount",
        "DecreaseCommandCount",
        "SetCommandTarget",
        "SetCommandType",
        "SetCommandCells",
        "RemoveCommandFromQueue",
        "DebugCommand",
        "ExecuteLuaInSim",
        "LuaSimCallback",
        "EndGame",
    ];
}

fn display_game_command_type(type_: &u8) -> std::borrow::Cow<'static, str> {
    game_command::NAMES
        .get(*type_ as usize)
        .map(|name| std::borrow::Cow::Borrowed(*name))
        .unwrap_or_else(|| std::borrow::Cow::Owned(format!("{}", type_)))
}

fn display_digest(digest: &[u8]) -> HexArray<'_> {
    HexArray(digest)
}

fn display_option<T: std::fmt::Display>(option: &Option<T>) -> TransparentOption<'_, T> {
    TransparentOption(option)
}

#[derive(Clone, Display, Debug, PartialEq)]
pub struct Position {
    /// East/West direction
    pub x: f32,
    /// Vertical direction
    pub y: f32,
    /// North/South direction
    pub z: f32,
}

pub mod target_type {
    pub const NONE: u8 = 0;
    pub const ENTITY: u8 = 1;
    pub const POSITION: u8 = 2;
}

#[derive(Clone, Display, Debug, PartialEq)]
pub enum Target {
    None,
    Entity {
        id: u32,
    },
    /// Where the top left corner of the map is (0, 0, 0)
    #[display(transparent)]
    Position(Position),
}

/// Quaternion representing orientation and a scale value
#[derive(Clone, Display, Debug, PartialEq)]
pub struct Formation {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
    pub scale: f32,
}

#[derive(Clone, Display, Debug, PartialEq)]
pub struct Cell {
    pub x: i16,
    pub z: i16,
}

/// The data contained by `IssueCommand` and `IssueFactoryCommand`. Some of the fields are
/// unknown as the documentation that used to exist on the gpg forums has been lost.
#[derive(Clone, Display, Debug, PartialEq)]
pub struct GameCommand {
    #[display(debug)]
    pub entity_ids: Vec<u32>,
    pub id: u32,
    /// References another command in the command queue. This is known to reference another attack
    /// command in the case of a "coordinated attack", but it may be used for other cases too.
    pub coordinated_attack_cmd_id: i32,
    #[display(name = "type", fmt_with = display_game_command_type)]
    pub type_: u8,
    pub arg2: i32,
    pub target: Target,
    pub arg3: u8,
    #[display(fmt_with = display_option)]
    pub formation: Option<Formation>,
    #[display(debug)]
    pub blueprint: String,
    pub arg4: u32,
    pub arg5: u32,
    pub arg6: u32,

    // Pretty consistently: 000080bf000080bf0201
    #[display(fmt_with = display_digest)]
    pub unknown: [u8; 8],
    /// A Lua table for selecting unit abilities.
    /// When command is Ability (45):
    /// ```json
    /// {
    ///     "AbilityId": "electroshock"
    /// }
    /// ```
    pub ability: LuaObject,
    /// Indicates whether or not starting this upgrade clears the current command queue.
    /// Only appears if `ability` is non empty.
    #[display(fmt_with = display_option)]
    pub clear_queue: Option<bool>,
    /// Appears to always be 1 when using an ability. Is it a bool?
    /// Does not exist when type == 2
    #[display(fmt_with = display_digest)]
    pub unknown2: Vec<u8>,
}

/// Constants refering to the `EUnitCommandType` enumeration
pub mod game_command {
    pub const NONE: u8 = 0;
    pub const STOP: u8 = 1;
    pub const COMMAND_2: u8 = 2;
    pub const COMMAND_3: u8 = 2;
    // Confirmed
    pub const MOVE: u8 = 4;
    pub const DIVE: u8 = 5;
    pub const FORM_MOVE: u8 = 6;
    pub const BUILD_SILO_TACTICAL: u8 = 7;
    pub const BUILD_SILO_NUKE: u8 = 8;
    pub const BUILD_FACTORY: u8 = 9;
    // Confirmed
    pub const BUILD_MOBILE: u8 = 10;
    pub const BUILD_ASSIST: u8 = 11;
    pub const ATTACK: u8 = 12;
    pub const FORM_ATTACK: u8 = 13;
    pub const NUKE: u8 = 14;
    pub const TACTICAL: u8 = 15;
    pub const TELEPORT: u8 = 16;
    pub const COMMAND_17: u8 = 17;
    pub const GUARD: u8 = 18;
    pub const PATROL: u8 = 19;
    pub const FERRY: u8 = 20;
    pub const COMMAND_21: u8 = 21;
    pub const FORM_PATROL: u8 = 22;
    pub const RECLAIM: u8 = 23;
    pub const REPAIR: u8 = 24;
    pub const CAPTURE: u8 = 25;
    pub const TRANSPORT_LOAD_UNITS: u8 = 26;
    pub const TRANSPORT_REVERSE_LOAD_UNITS: u8 = 27;
    pub const TRANSPORT_UNLOAD_UNITS: u8 = 28;
    pub const TRANSPORT_UNLOAD_SPECIFIC_UNITS: u8 = 29;
    pub const DETACH_FROM_TRANSPORT: u8 = 30;
    pub const UPGRADE: u8 = 31;
    pub const SCRIPT: u8 = 32;
    pub const ASSIST_COMMANDER: u8 = 33;
    pub const KILL_SELF: u8 = 34;
    pub const DESTROY_SELF: u8 = 35;
    pub const SACRIFICE: u8 = 36;
    pub const PAUSE: u8 = 37;
    pub const OVER_CHARGE: u8 = 38;
    pub const AGGRESSIVE_MOVE: u8 = 39;
    pub const FORM_AGGRESSIVE_MOVE: u8 = 40;
    pub const ASSIST_MOVE: u8 = 41;
    pub const SPECIAL_ACTION: u8 = 42;
    pub const DOCK: u8 = 43;
    pub const COMMAND_44: u8 = 44;
    pub const ABILITY: u8 = 45;
    /// The maximum valid command type
    pub const MAX: u8 = 45;

    pub static NAMES: [&str; MAX as usize + 1] = [
        "None",
        "Stop",
        // Pretty common, has a target: Position.
        "2",
        "3",
        "Move",
        "Dive",
        "FormMove",
        "BuildSiloTactical",
        "BuildSiloNuke",
        "BuildFactory",
        "BuildMobile",
        "BuildAssist",
        "Attack",
        "FormAttack",
        "Nuke",
        "Tactical",
        "Teleport",
        // Has blueprint, often ucb0001. Somtimes empty selection
        "17",
        "Guard",
        "Patrol",
        "Ferry",
        // Has entity type target
        "21",
        "FormPatrol",
        "Reclaim",
        "Repair",
        "Capture",
        "TransportLoadUnits",
        "TransportReverseLoadUnits",
        "TransportUnloadUnits",
        "TransportUnloadSpecificUnits",
        "DetachFromTransport",
        "Upgrade",
        "Script",
        "AssistCommander",
        "KillSelf",
        "DestroySelf",
        "Sacrifice",
        "Pause",
        "OverCharge",
        "AggressiveMove",
        "FormAggressiveMove",
        "AssistMove",
        "SpecialAction",
        "Dock",
        "44",
        "Ability",
    ];
}
