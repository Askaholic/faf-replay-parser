use ordered_hash_map::OrderedHashMap;
use std::fmt;

pub struct DebugAsDisplay<T: fmt::Display>(pub T);
impl<T: fmt::Display> fmt::Debug for DebugAsDisplay<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}

pub struct DebugKeyDisplayValueMap<'a, K: fmt::Debug, V: fmt::Display>(
    pub &'a OrderedHashMap<K, V>,
);
impl<'a, K, V> fmt::Display for DebugKeyDisplayValueMap<'a, K, V>
where
    K: fmt::Debug,
    V: fmt::Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map()
            .entries(self.0.iter().map(|(k, v)| (k, DebugAsDisplay(v))))
            .finish()
    }
}
