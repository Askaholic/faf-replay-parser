use crate::display::DebugKeyDisplayValueMap;
use crate::version_detector::VersionProxy;
use crate::{format_duration, load_replay, print_replay_version, MISSING};

use faf_replay_parser::aggregator::*;
use faf_replay_parser::iter::parse_command;
use faf_replay_parser::iter::prelude::*;
use faf_replay_parser::parser::parse_header;
use faf_replay_parser::replay::{ReplayHeader, SimData};
use faf_replay_parser::version::{Command, CommandId};
use faf_replay_parser::{ReplayReadError, ReplayResult, Version, SC2, SCFA};

use clap::Args;
use rayon::prelude::*;
use regex::Regex;

use std::borrow::Cow;
use std::collections::HashMap;
use std::error::Error;
use std::io::Cursor;
use std::path::PathBuf;
use std::time::Duration;

#[derive(Args)]
pub struct InfoArgs {
    /// Path to the replay, can be multiple
    #[arg(required = true)]
    replays: Vec<PathBuf>,
    // TODO: replace with --scfa/--sc2
    /// Which version of Supreme Commander to process the replay as
    ///
    /// If ommitted, this will be guessed from the replay header. Guesses will not always be
    /// correct.
    #[arg(value_enum, long)]
    version: Option<VersionProxy>,
    /// Enable all detailed output
    #[arg(short, long, conflicts_with_all = ["PrettyOutput", "raw"])]
    all: bool,
    /// Print the entire header as it appears in the file
    #[arg(short, long, conflicts_with_all = ["PrettyOutput", "all"])]
    raw: bool,

    #[command(flatten)]
    pretty_output: PrettyOutput,
}
impl InfoArgs {
    /// A convenience for checking if an option is enabled
    fn enabled(&self, option: bool) -> bool {
        self.all || option
    }
}

#[derive(Args)]
#[group(multiple = true)]
struct PrettyOutput {
    /// Print additional details about the map
    #[arg(long = "map")]
    map_info: bool,
    /// Print additional details about the game options
    #[arg(long = "options")]
    option_info: bool,
    /// Print additional details about the mods used
    #[arg(long = "mods")]
    mods_info: bool,
    /// Print additional details about the players
    #[arg(long = "players")]
    player_info: bool,
    /// Print every beat number on which a desync occurred
    #[arg(long = "desyncs")]
    desyncs: bool,
}

/// Any info outputted by this command.
struct ReplayInfo {
    version: VersionProxy,
    header: ReplayHeader,
    body_info: Option<ReplayBodyInfo>,
    map_info: Option<ReplayMapInfo>,
    players_info: Option<HashMap<String, ReplayPlayerInfo>>,
    options_info: Option<ReplayOptionsInfo>,
    mods_info: Option<Vec<ReplayModInfo>>,
}

struct ReplayBodyInfo {
    sim: SimData,
}

/// Subcommand `info`
pub fn print_info(args: &InfoArgs) -> Result<(), Box<dyn Error>> {
    let replay_infos: Vec<ReplayResult<ReplayInfo>> = args
        .replays
        .par_iter()
        .map(|path| {
            let replay_data = load_replay(path)?;
            let mut cur = Cursor::new(&replay_data);

            let header = parse_header(&mut cur)?;
            let body_start = cur.position() as usize;
            let body_data = &replay_data[body_start..];
            let version = args
                .version
                .unwrap_or_else(|| VersionProxy::from_header(&header));

            if args.raw {
                return Ok(ReplayInfo {
                    version,
                    header,
                    body_info: None,
                    map_info: None,
                    players_info: None,
                    options_info: None,
                    mods_info: None,
                });
            }

            let body_info = match version {
                VersionProxy::Scfa => get_body_info::<SCFA>(body_data)?,
                VersionProxy::Sc2 => get_body_info::<SC2>(body_data)?,
            };

            // TODO: Only aggregate if that portion is enabled
            let map_info = aggregate_replay_map_info(&header);
            let players_info = aggregate_replay_players_info(&header);
            let options_info = aggregate_replay_options_info(&header);
            let mods_info = aggregate_replay_mods_info(&header);

            Ok(ReplayInfo {
                version,
                header,
                body_info: Some(body_info),
                map_info: Some(map_info),
                players_info: Some(players_info),
                options_info: Some(options_info),
                mods_info: Some(mods_info),
            })
        })
        .collect();

    ReplayInfo::print_human_readable_list(&replay_infos, &args);

    Ok(())
}

fn get_body_info<V: Version>(data: &[u8]) -> ReplayResult<ReplayBodyInfo> {
    let commands = V::CommandId::builder_defaults()
        .map(|cmd| cmd.as_u8())
        .collect::<Vec<u8>>();
    let mut sim = SimData::new();
    for result in data.iter_command_frames::<V>() {
        let frame = result?;
        if !commands.contains(&frame.cmd) {
            continue;
        }
        let cmd = parse_command::<V>(frame)?;
        match V::Command::process_command(&mut sim, &cmd) {
            Err(ReplayReadError::Desynced(_)) => (),
            result => result?,
        }
    }

    Ok(ReplayBodyInfo { sim })
}

// Not doing this with traits for now
impl ReplayInfo {
    fn print_human_readable(&self, args: &InfoArgs) {
        if args.raw {
            println!("SupCom version: {:?}", self.header.scfa_version);
            println!("Replay version: {:?}", self.header.replay_version);
            println!("Map file: {:?}", self.header.map_file);
            println!("Mods: {:#}", self.header.mods);
            println!("Scenario: {:#}", self.header.scenario);
            println!(
                "Players: {:#}",
                DebugKeyDisplayValueMap(&self.header.players)
            );
            println!("Cheats enabled: {:?}", self.header.cheats_enabled);
            println!("Armies: {:#}", DebugKeyDisplayValueMap(&self.header.armies));
            println!("Seed: {:?}", self.header.seed);
            return;
        }

        print_replay_version(&self.header);
        if let Some(options_info) = &self.options_info {
            if let Some(title) = &options_info.title {
                println!("\nTitle: {}", title);
            }
            if let Some(quality) = &options_info.quality {
                println!("Quality: {}%", quality);
            }
        }

        if let Some(body_info) = &self.body_info {
            let i18n_regex = Regex::new(r"<.*>").unwrap();
            if let Some(tick) = body_info.sim.desync_tick {
                println!("\nWARNING: Replay desynced at tick {}", tick);
                if args.enabled(args.pretty_output.desyncs) {
                    println!(
                        "All desynced tick: {:?}",
                        body_info.sim.desync_ticks.as_ref().unwrap()
                    );
                }
            }
            println!(
                "\n{} ({})",
                self.map_info
                    .as_ref()
                    .and_then(|info| info
                        .name
                        .as_ref()
                        .map(|n| i18n_regex.replace_all(&n, "").into_owned()))
                    .unwrap_or("Unknown Map".to_string()),
                format_duration(&Duration::from_millis(body_info.sim.tick as u64 * 100))
            );
            println!(
                "    {}",
                self.map_info
                    .as_ref()
                    .and_then(|info| info
                        .description
                        .as_ref()
                        .map(|d| i18n_regex.replace_all(&d, "").into_owned()))
                    .unwrap_or("No description provided".to_string())
            );
        }

        self.map_info.as_ref().map(|info| print_map(args, info));
        self.options_info
            .as_ref()
            .map(|info| print_options(args, info));
        self.mods_info.as_ref().map(|info| print_mods(args, info));
        self.players_info.as_ref().map(|info| {
            println!();
            print_players(args, info, &self.version);
        });
    }

    fn print_human_readable_list(infos: &[ReplayResult<ReplayInfo>], args: &InfoArgs) {
        if !infos.is_empty() {
            println!();
        }
        for (info, path) in infos.iter().zip(&args.replays) {
            println!("{}", path.display());
            match info {
                Ok(info) => {
                    info.print_human_readable(args);
                    println!();
                }
                Err(e) => println!("Error parsing replay: {}", e),
            }
        }
    }
}

fn print_map(args: &InfoArgs, map_info: &ReplayMapInfo) {
    if args.enabled(args.pretty_output.map_info) {
        println!("\nMap");
        map_info
            .preview
            .as_ref()
            .map(|s| println!("    Preview: {}", s));
        println!("    File: {}", &map_info.map_file.display());
        map_info
            .scenario_file
            .as_ref()
            .map(|s| println!("    Scenario: {}", s.display()));
        map_info
            .script_file
            .as_ref()
            .map(|s| println!("    Script: {}", s.display()));
        map_info
            .save_file
            .as_ref()
            .map(|s| println!("    Save: {}", s.display()));
    }
}

fn print_options(args: &InfoArgs, options_info: &ReplayOptionsInfo) {
    if args.enabled(args.pretty_output.option_info) {
        println!("\nOptions");
        options_info
            .victory
            .as_ref()
            .map(|s| println!("    Victory: {}", s));
        options_info
            .share
            .as_ref()
            .map(|s| println!("    Share: {}", s));
        options_info
            .score
            .as_ref()
            .map(|s| println!("    Score: {}", s));
        options_info
            .unit_cap
            .map(|i| println!("    Unit Cap: {}", i));
        options_info
            .no_rush
            .as_ref()
            .map(|s| println!("    No Rush: {}", s));
        options_info
            .game_speed
            .as_ref()
            .map(|s| println!("    Game Speed: {}", s));
        options_info
            .cheats
            .map(|b| println!("    CheatsEnabled: {}", b));
        if options_info.cheats.unwrap_or(true) {
            options_info
                .build_mult
                .map(|f| println!("    BuildMult: {}", f));
            options_info
                .cheat_mult
                .map(|f| println!("    CheatMult: {}", f));
        }
    }
}

fn print_mods(args: &InfoArgs, mods_info: &[ReplayModInfo]) {
    if !mods_info.is_empty() || args.enabled(args.pretty_output.mods_info) {
        println!("\nMods");
    }

    for mod_info in mods_info {
        println!(
            "    {}{}",
            mod_info
                .name
                .as_ref()
                .map(String::as_str)
                .unwrap_or("Unknown Mod"),
            mod_info
                .version
                .map(|v| format!(" v{}", v))
                .unwrap_or("".into())
        );
        if args.enabled(args.pretty_output.mods_info) {
            mod_info
                .description
                .as_ref()
                .map(|desc| println!("        {}", desc));
            mod_info
                .author
                .as_ref()
                .map(|author| println!("        Author: {}", author));
            mod_info
                .copyright
                .as_ref()
                .map(|copy| println!("        Copyright: {}", copy));
            mod_info
                .uid
                .as_ref()
                .map(|uid| println!("        UID: {}", uid));
            mod_info
                .url
                .as_ref()
                .map(|url| println!("        URL: {}", url));
            mod_info
                .location
                .as_ref()
                .map(|location| println!("        Location: {}", location.display()));
        }
    }
}

fn print_players(
    args: &InfoArgs,
    players_info: &HashMap<String, ReplayPlayerInfo>,
    version: &VersionProxy,
) {
    type Team<'a> = Vec<&'a ReplayPlayerInfo>;

    let mut teams: HashMap<i64, Team> = HashMap::new();

    for (_name, info) in players_info {
        let team = info.team.map_or(-1, |t| t as i64);
        match teams.contains_key(&team) {
            true => teams.get_mut(&team).unwrap().push(info),
            false => {
                teams.insert(team, Vec::new());
                teams.get_mut(&team).unwrap().push(info);
            }
        };
    }

    for (_num, team) in teams.iter_mut() {
        team.sort_unstable_by_key(|p| {
            p.army_name
                .as_ref()
                .map(|n| Cow::Owned(n.to_string()))
                .unwrap_or_else(|| {
                    p.name
                        .as_ref()
                        .map(|n| Cow::Owned(n.to_string()))
                        .unwrap_or(Cow::from(""))
                })
        });
    }

    let mut teams: Vec<(i64, Team)> = teams.drain().collect();
    teams.sort_unstable_by_key(|t| t.0);

    let clan_max_len = players_info
        .values()
        .map(|p| p.clan.as_deref().map(str::len).unwrap_or(0))
        .max()
        .unwrap_or(0);

    for (num, team) in teams {
        println!("Team {}", num);
        for player in team {
            print_player(args, &player, clan_max_len, version);
        }
        println!();
    }
}

fn print_player(
    args: &InfoArgs,
    player: &ReplayPlayerInfo,
    clan_max_len: usize,
    version: &VersionProxy,
) {
    // Used for excluding fields from the output
    let treat_as_human = player.human.unwrap_or(true);
    // Player name
    print!("    ");
    if let Some(ref clan) = player.clan {
        print!("[{}] ", clan);
        if clan.len() < clan_max_len {
            print!("{}", " ".repeat(clan_max_len - clan.len()));
        }
    } else if clan_max_len > 0 && !args.enabled(args.pretty_output.player_info) {
        print!(" {}  ", " ".repeat(clan_max_len));
    }
    print!(
        "{}",
        player
            .name
            .as_ref()
            .map(String::as_str)
            .unwrap_or("Unknown Player"),
    );

    if treat_as_human {
        if let Some(rating) = player.rating {
            print!(" ({})", rating);
        } else {
            print!(" :");
        }
    } else {
        print!(
            " ({})",
            player
                .ai_personality
                .as_ref()
                .map(String::as_str)
                .unwrap_or("AI")
        );
    }
    print!(
        " {}",
        player
            .faction
            .map(|f| version
                .faction_name_lookup()
                .get(f as usize - 1)
                .map(|f| Cow::from(*f))
                .unwrap_or(Cow::from(f.to_string())))
            .unwrap_or(Cow::from("Missing Faction"))
    );
    println!();
    // End player name

    // Player details
    if args.enabled(args.pretty_output.player_info) {
        if treat_as_human {
            player.id.map(|id| println!("        Id: {}", id));
            player
                .country
                .as_ref()
                .map(|s| println!("        Country: {}", s.to_ascii_uppercase()));
            if player.mean.is_some() || player.deviation.is_some() {
                println!(
                    "        Trueskill: {} \u{00b1} {}",
                    player
                        .mean
                        .map(|m| Cow::from(format!("{:.0}", m)))
                        .unwrap_or(Cow::from(MISSING)),
                    player
                        .deviation
                        .map(|m| Cow::from(format!("{:.0}", m)))
                        .unwrap_or(Cow::from(MISSING)),
                );
            }
        } else {
            player.civilian.map(|b| println!("        Civilian: {}", b));
        }
        player.human.map(|b| println!("        Human: {}", b));
        player
            .start_spot
            .map(|s| println!("        Start Spot: {}", s));
        player
            .army_name
            .as_ref()
            .map(|s| println!("        Army Name: {}", s));
        player
            .army_color
            .or(player.player_color)
            .map(|c| println!("        Color: {}", c));
        if treat_as_human {
            player.ng.map(|i| println!("        Games Played: {}", i));
            player.pl.map(|i| println!("        PL: {}", i));
            player.rc.as_ref().map(|s| println!("        RC: {}", s));
        }
    }
}
