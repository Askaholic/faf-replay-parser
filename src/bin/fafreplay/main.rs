mod commands;
mod display;
mod info;
mod unpack;
mod version_detector;

use commands::{print_commands, CommandsArgs};
use info::{print_info, InfoArgs};
use unpack::{unpack_fafreplay, UnpackArgs};
use version_detector::{guess_file_format, FileFormat};

use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;
use std::time::Duration;

use clap::builder::Styles;
use clap::{Parser, Subcommand};

use faf_replay_parser::faf::extract_scfa;
use faf_replay_parser::replay::ReplayHeader;
use faf_replay_parser::{ReplayReadError, ReplayResult};

const MISSING: &str = "[MISSING]";

#[derive(Parser)]
#[command(
    name = "FAF Replay Parser",
    about = "Parse Supreme Commander Forged Alliance replay files",
    styles = Styles::plain(),
    version,
)]
struct Cli {
    #[command(subcommand)]
    cmd: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Show an overview of the replay contents
    Info(InfoArgs),
    /// Display the parsed command data directly.
    ///
    /// By default this only shows the most common commands.
    Commands(CommandsArgs),
    /// Convert a .fafreplay into a .scfareplay
    Unpack(UnpackArgs),
}

fn main() {
    let args = Cli::parse();

    if let Err(e) = match args.cmd {
        Commands::Info(args) => print_info(&args),
        Commands::Commands(args) => print_commands(&args),
        Commands::Unpack(args) => unpack_fafreplay(&args),
    } {
        if let Some(e) = e.downcast_ref::<ReplayReadError>() {
            println!("Error parsing replay: {}", e);
        } else {
            println!("Error: {}", e);
        }
        let mut outer = &*e;
        while let Some(inner) = outer.source() {
            println!("Caused by: {}", e);
            outer = inner;
        }
        std::process::exit(2);
    }
}

fn load_replay(path: &Path) -> ReplayResult<Vec<u8>> {
    println!("processing replay: {}", path.to_string_lossy());

    let mut buf = Vec::new();
    let mut reader = BufReader::new(File::open(path)?);
    match guess_file_format(&mut reader, &mut buf)? {
        Some(FileFormat::ScReplay) => {
            reader.read_to_end(&mut buf)?;
            Ok(buf)
        }
        Some(FileFormat::Faf) => {
            reader.read_to_end(&mut buf)?;
            let mut extracted = Vec::with_capacity(buf.len());
            extract_scfa(&mut buf.as_slice(), &mut extracted)?;
            Ok(extracted)
        }
        None => Err(ReplayReadError::IO(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "unknown file type",
        ))),
    }
}

fn print_replay_version(header: &ReplayHeader) {
    println!("{} {}", header.scfa_version, header.replay_version);
}

fn format_duration(duration: &Duration) -> String {
    let total_secs = duration.as_secs();
    let (hours, rem) = (total_secs / 3600, total_secs % 3600);
    let (minutes, seconds) = (rem / 60, rem % 60);
    format!("{:02}:{:02}:{:02}", hours, minutes, seconds)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Cli::command().debug_assert();
    }
}
