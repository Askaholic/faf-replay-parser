use crate::version_detector::VersionProxy;
use crate::{format_duration, load_replay, print_replay_version, Cli};

use std::error::Error;
use std::io::Cursor;
use std::path::PathBuf;
use std::time::Duration;

use faf_replay_parser::iter::prelude::*;
use faf_replay_parser::iter::{parse_command, verify_frame_header};
use faf_replay_parser::parser::parse_header;
use faf_replay_parser::version::{Advance, Command, CommandId};
use faf_replay_parser::{sc2, scfa};
use faf_replay_parser::{ReplayResult, Version, SC2, SCFA};

use clap::error::ErrorKind;
use clap::{Args, CommandFactory};

#[derive(Args)]
pub struct CommandsArgs {
    /// Path to the replay
    replay: PathBuf,
    // TODO: replace with --scfa/--sc2
    /// Which version of Supreme Commander to process the replay as
    ///
    /// If ommitted, this will be guessed from the replay header. Guesses will not always be
    /// correct.
    #[arg(value_enum, long)]
    version: Option<VersionProxy>,
    /// Limits output to the first <num> commands
    #[arg(long)]
    limit: Option<usize>,
    /// Parses all command types
    #[arg(short, long)]
    all: bool,
    /// Parses these command types
    #[arg(short, long = "commands")]
    commands: Vec<String>,
    /// Does not parse these commands
    ///
    /// Command exclusion is always processed after command inclusion, so listing a command in both
    /// the include and exclude list will result in the command not being parsed. You probably want
    /// to use this along with --all.
    #[arg(short = 'e', long = "exclude")]
    exclude_commands: Vec<String>,
    /// Shows the in game time before each command
    ///
    /// Causes Advance commands to be parsed implicitly even if they are not displayed.
    #[arg(short, long)]
    time: bool,
    /// Print hexidecimal file offsets for each command
    #[arg(short = 'x', long = "offset")]
    offset: bool,
    /// Don't print the commands, only the total number that were found
    #[arg(long)]
    count_only: bool,
}

fn linear_search<T: PartialEq>(slice: &[T], item: &T) -> Option<usize> {
    for (i, t) in slice.iter().enumerate() {
        if t == item {
            return Some(i);
        }
    }
    return None;
}

static DEFAULT_INCLUDE_COMMANDS_SCFA: [u8; 6] = [
    scfa::replay_command::ADVANCE,
    scfa::replay_command::SET_COMMAND_SOURCE,
    scfa::replay_command::COMMAND_SOURCE_TERMINATED,
    scfa::replay_command::VERIFY_CHECKSUM,
    scfa::replay_command::LUA_SIM_CALLBACK,
    scfa::replay_command::END_GAME,
];

static DEFAULT_INCLUDE_COMMANDS_SC2: [u8; 6] = [
    sc2::replay_command::ADVANCE,
    sc2::replay_command::SET_COMMAND_SOURCE,
    sc2::replay_command::COMMAND_SOURCE_TERMINATED,
    sc2::replay_command::VERIFY_CHECKSUM,
    sc2::replay_command::LUA_SIM_CALLBACK,
    sc2::replay_command::END_GAME,
];

/// Subcommand 'commands'
pub fn print_commands(args: &CommandsArgs) -> Result<(), Box<dyn Error>> {
    let replay_data = load_replay(&args.replay)?;

    // Parse the replay
    let mut cur = Cursor::new(replay_data.as_slice());
    let header = parse_header(&mut cur)?;
    let body_start = cur.position() as usize;
    let body_data = &replay_data[body_start..];
    let version = args
        .version
        .unwrap_or_else(|| VersionProxy::from_header(&header));

    let include = build_include_table(&version, args)?;

    // Print info
    print_replay_version(&header);
    println!();

    match version {
        VersionProxy::Scfa => print_command_stream::<SCFA>(args, &include, body_data, body_start),
        VersionProxy::Sc2 => print_command_stream::<SC2>(args, &include, body_data, body_start),
    }?;

    Ok(())
}

fn build_include_table(version: &VersionProxy, args: &CommandsArgs) -> Result<Vec<bool>, String> {
    let mut include = vec![false; version.command_max() as usize + 1];

    let lookup = &version.command_name_lookup();
    let mut cmd = Cli::command();

    // Add commands from args
    if args.all {
        include.fill(true);
    } else if args.commands.len() > 0 {
        for name in &args.commands {
            match linear_search(lookup, &name.as_str()) {
                Some(idx) => include[idx] = true,
                None => {
                    cmd.error(
                        ErrorKind::InvalidValue,
                        format!(
                            "invalid value '{}' for '--commands'\n    possible values: {:?}",
                            name, lookup
                        ),
                    )
                    .exit();
                }
            }
        }
    } else {
        for cmd in get_default_include_commands(version) {
            include[*cmd as usize] = true;
        }
    }

    for name in &args.exclude_commands {
        match linear_search(lookup, &name.as_str()) {
            Some(idx) => include[idx] = false,
            None => {
                cmd.error(
                    ErrorKind::InvalidValue,
                    format!(
                        "invalid value '{}' for '--exclude'\n    possible values: {:?}",
                        name, lookup
                    ),
                )
                .exit();
            }
        }
    }

    Ok(include)
}

fn get_default_include_commands(version: &VersionProxy) -> &'static [u8] {
    match version {
        VersionProxy::Scfa => &DEFAULT_INCLUDE_COMMANDS_SCFA,
        VersionProxy::Sc2 => &DEFAULT_INCLUDE_COMMANDS_SC2,
    }
}

fn print_command_stream<V: Version>(
    args: &CommandsArgs,
    include: &[bool],
    data: &[u8],
    offset: usize,
) -> ReplayResult<()> {
    let advance = V::CommandId::advance().as_u8();

    if args.time && !include[advance as usize] {
        println!("Time option used without including Advance commands, Advance will be parsed implicitly\n");
    }
    if args.count_only {
        println!("Count only mode enabled, no command data will be displayed");
    }

    let mut position = offset;
    let mut total_parsed = 0;
    let mut total_ticks = 0;
    for (frame, offset) in data
        .iter_command_frames_raw()
        .map(|frame| {
            let offset = position;
            position += frame.data.len();
            (frame, offset)
        })
        .filter(|(frame, _)| {
            frame.cmd as usize >= include.len()
                || include[frame.cmd as usize]
                || frame.cmd == advance && args.time
        })
        .enumerate()
        .take_while(|(i, _)| args.limit.map(|limit| *i < limit).unwrap_or(true))
        .map(|(_, frame)| frame)
    {
        let cmd = parse_command::<V>(verify_frame_header::<V>(frame)?)?;
        total_parsed += 1;

        if let Some(Advance { ticks }) = cmd.as_advance() {
            total_ticks += ticks as u64;

            if !include[advance as usize] {
                continue;
            }
        }

        if !args.count_only {
            if args.offset {
                print!("{:#010x} ", offset);
            }
            if args.time {
                print!(
                    "{} ",
                    format_duration(&Duration::from_millis(total_ticks * 100))
                );
            }
            println!("├── {}", cmd);
        }
    }
    println!();
    println!("Total commands parsed: {}", total_parsed);

    Ok(())
}
