use faf_replay_parser::replay::ReplayHeader;
use faf_replay_parser::version::CommandId;
use faf_replay_parser::{sc2, scfa, ReplayResult, Version, SC2, SCFA};

use clap::ValueEnum;
use regex::Regex;

use std::io::{BufRead, Read};

const SCFA_FACTION: [&str; 4] = ["UEF", "Aeon", "Cybran", "Seraphim"];
const SC2_FACTION: [&str; 3] = ["UEF", "Illuminate", "Cybran"];

/// Enables dynamically switching between Version implementations
#[derive(Clone, Copy, ValueEnum)]
pub enum VersionProxy {
    Scfa,
    Sc2,
}
impl VersionProxy {
    pub fn from_header(header: &ReplayHeader) -> Self {
        Self::from_scfa_version(&header.scfa_version)
    }

    pub fn from_scfa_version(scfa_version: &str) -> Self {
        let re = Regex::new(r"Supreme Commander v1\.\d+(?P<patch>\.\d+)?").unwrap();
        re.captures(scfa_version)
            .map(|caps| match caps.name("patch") {
                Some(_) => VersionProxy::Scfa,
                None => VersionProxy::Sc2,
            })
            .unwrap_or(VersionProxy::Scfa)
    }

    pub fn faction_name_lookup(&self) -> &'static [&'static str] {
        match self {
            Self::Scfa => &SCFA_FACTION,
            Self::Sc2 => &SC2_FACTION,
        }
    }

    pub fn command_max(&self) -> u8 {
        match self {
            Self::Scfa => <SCFA as Version>::CommandId::max(),
            Self::Sc2 => <SC2 as Version>::CommandId::max(),
        }
    }

    pub fn command_name_lookup(&self) -> &'static [&'static str] {
        match self {
            Self::Scfa => &scfa::replay_command::NAMES,
            Self::Sc2 => &sc2::replay_command::NAMES,
        }
    }
}

/// Enables detecting the file format automatically.
pub enum FileFormat {
    ScReplay,
    Faf,
}

/// Guess whether this is a replay file produced by the game or a FAF wrapper format by looking at
/// the first few bytes.
///
/// Will produce false positives for JSON files, and in rare cases, text files.
pub fn guess_file_format(
    mut reader: impl Read + BufRead,
    mut buf: &mut Vec<u8>,
) -> ReplayResult<Option<FileFormat>> {
    buf.resize(19, 0);

    reader.read_exact(&mut buf)?;

    if buf == b"Supreme Commander v" {
        return Ok(Some(FileFormat::ScReplay));
    } else if &buf[0..1] == b"{" {
        return Ok(Some(FileFormat::Faf));
    }

    Ok(None)
}
