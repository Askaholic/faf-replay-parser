use crate::reader::ReplayReadError;
use crate::replay::ReplayCommandFrameSpan;
use crate::version::Version;
use crate::ReplayResult;
use std::convert::TryFrom;

/// Checks that the command id and size field are valid. This is intended to be used with the
/// [`BytesCommandIter`](trait.BytesCommandIter.html) trait.
///
/// # Errors
/// If either check fails, a `ReplayReadError::Malformed` is returned.
pub fn verify_frame_header<V: Version>(
    frame: ReplayCommandFrameSpan,
) -> ReplayResult<ReplayCommandFrameSpan> {
    if let Err(_) = V::CommandId::try_from(frame.cmd) {
        return Err(ReplayReadError::Malformed("invalid command"));
    }
    if frame.size < 3 {
        return Err(ReplayReadError::Malformed("invalid command size"));
    }
    return Ok(frame);
}

/// An iterator adapter for checking the validity of command frame headers.
///
/// See [`verify_frame_header`].
pub struct VerifyFrame<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    iter: I,
    phantom: std::marker::PhantomData<V>,
}
impl<'a, V, I> VerifyFrame<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    pub fn new(iter: I) -> VerifyFrame<'a, V, I> {
        VerifyFrame {
            iter,
            phantom: std::marker::PhantomData,
        }
    }
}

impl<'a, V, I> Iterator for VerifyFrame<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    type Item = ReplayResult<ReplayCommandFrameSpan<'a>>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(verify_frame_header::<V>)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
