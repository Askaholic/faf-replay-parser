use crate::iter::verify_frame_header;
use crate::replay::ReplayCommandFrameSpan;
use crate::version::{Command, Version};
use crate::ReplayResult;

/// Parse some command data into a `Command`. This is intended to be used with the
/// [`BytesCommandIter`](trait.BytesCommandIter.html) trait.
///
/// # Errors
/// Returns a `ReplayReadError` if the command data can't be parsed or is too short.
pub fn parse_command<V: Version>(frame: ReplayCommandFrameSpan) -> ReplayResult<V::Command> {
    // This validation already happens during parsing as a necessity
    debug_assert!(frame.data.len() >= 3);

    V::Command::parse_command_data(frame.cmd, &frame.data[3..])
}

/// An iterator adapter for parsing command frame headers into `Command`s. This also verifies
/// the contents of the frame headers first, returning any errors.
///
/// See [`CommandFrameAdapters::parse_commands`](trait.CommandFrameAdapters.html#method.parse_commands).
pub struct ParseCommand<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    iter: I,
    phantom: std::marker::PhantomData<V>,
}
impl<'a, V, I> ParseCommand<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    pub fn new(iter: I) -> ParseCommand<'a, V, I> {
        ParseCommand {
            iter,
            phantom: std::marker::PhantomData,
        }
    }
}

impl<'a, V, I> Iterator for ParseCommand<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    type Item = ReplayResult<V::Command>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.iter
            .next()
            .map(verify_frame_header::<V>)
            .map(|result| result.and_then(parse_command::<V>))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}

/// An iterator adapter for parsing command frame headers into `Command`s.
///
/// Note that this does not verify the validity of the command frame headers.
///
/// See [`CommandFrameAdapters::parse_command_data`](trait.CommandFrameAdapters.html#method.parse_command_data).
pub struct ParseCommandData<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    iter: I,
    phantom: std::marker::PhantomData<V>,
}
impl<'a, V, I> ParseCommandData<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    pub fn new(iter: I) -> ParseCommandData<'a, V, I> {
        ParseCommandData {
            iter,
            phantom: std::marker::PhantomData,
        }
    }
}

impl<'a, V, I> Iterator for ParseCommandData<'a, V, I>
where
    V: Version,
    I: Iterator<Item = ReplayCommandFrameSpan<'a>>,
{
    type Item = ReplayResult<V::Command>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(parse_command::<V>)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}
