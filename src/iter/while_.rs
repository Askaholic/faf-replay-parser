/// An iterator adapter for unwraping `Ok` values and stopping on the first `Err`.
pub struct WhileOk<I> {
    iter: I,
}

impl<I> WhileOk<I> {
    pub fn new(iter: I) -> WhileOk<I> {
        WhileOk { iter }
    }
}

impl<I, T, U> Iterator for WhileOk<I>
where
    I: Iterator<Item = Result<T, U>>,
{
    type Item = T;

    // Inlining here seems to be BAD for performance?
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().and_then(|f| f.ok())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let (_, upper) = self.iter.size_hint();
        (0, upper)
    }
}

/// An iterator adapter for unwraping `Err` values and stopping on the first `Ok`.
pub struct WhileErr<I> {
    iter: I,
}

impl<I> WhileErr<I> {
    pub fn new(iter: I) -> WhileErr<I> {
        WhileErr { iter }
    }
}

impl<I, T, U> Iterator for WhileErr<I>
where
    I: Iterator<Item = Result<T, U>>,
{
    type Item = U;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().and_then(|f| f.err())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let (_, upper) = self.iter.size_hint();
        (0, upper)
    }
}
