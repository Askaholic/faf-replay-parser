//! Traits that expose the iter functionality.

pub use super::CommandFrameAdapters;
pub use super::CommandIter;
pub use super::ResultAdapters;
