//! Handling of lua types

use ordered_hash_map::OrderedHashMap;

use std::ffi::{CStr, CString, IntoStringError};
use std::fmt::{Debug, Display};
use std::hash::{Hash, Hasher};
use std::string::FromUtf8Error;

/// Byte marking that the serialized object is a Float
pub const LUA_FLOAT_MARKER: u8 = 0;
/// Byte marking that the serialized object is a String
pub const LUA_STRING_MARKER: u8 = 1;
/// Byte marking that the serialized object is Nil
pub const LUA_NIL_MARKER: u8 = 2;
/// Byte marking that the serialized object is a Boolean
pub const LUA_BOOL_MARKER: u8 = 3;
/// Byte marking that the serialized object is a Table
pub const LUA_TABLE_MARKER: u8 = 4;
/// Byte marking the end of a serialized Table
pub const LUA_END_MARKER: u8 = 5;

pub type LuaTable = OrderedHashMap<LuaObject, LuaObject>;

/// Provides a simple syntax for constructing lua objects.
///
/// *This was inspired by the very similar `json!` macro from `serde_json`.*
///
/// # Examples
/// ```rust
/// use faf_replay_parser::{lua, lua::LuaObject};
///
/// // Basic types
/// let obj1: LuaObject = lua!(10.0);
/// let obj2: LuaObject = lua!(nil);
/// let obj3: LuaObject = lua!(true);
/// // By default, strings are converted to `LuaObject::Unicode`
/// let obj4: LuaObject = lua!("foo");
/// // `LuaObject::String`s need to be explicitly prefixed
/// let obj5: LuaObject = lua!(cstr "foo");
///
/// // Tables use lua syntax
/// let table1: LuaObject = lua! {
///     foo = "bar",
///     // Expression evaluation is supported
///     bar = 2.0 + 3.0,
/// };
/// let table2: LuaObject = lua! {
///     ["foo"] = "bar",
///     [10.] = true,
///     // Trailing comma optional
///     [false] = 100.
/// };
/// ```
#[macro_export]
macro_rules! lua {
    () => (
        $crate::lua::LuaObject::Nil
    );
    (nil) => (
        $crate::lua::LuaObject::Nil
    );
    ($item:expr) => (
        $crate::lua::LuaObject::from($item)
    );
    (cstr $item:literal) => (
        $crate::lua::LuaObject::from(std::ffi::CString::new($item).unwrap())
    );
    ($($key:ident = $val:expr),* $(,)?) => (
        $crate::lua::LuaObject::Table(
            <ordered_hash_map::OrderedHashMap<$crate::lua::LuaObject, $crate::lua::LuaObject> as std::iter::FromIterator<($crate::lua::LuaObject, $crate::lua::LuaObject)>>::from_iter([
                $(
                    (lua!(stringify!($key)), lua!($val))
                ),*
            ])
        )
    );
    ($([$key:expr] = $val:expr),* $(,)?) => (
        $crate::lua::LuaObject::Table(
            <ordered_hash_map::OrderedHashMap<$crate::lua::LuaObject, $crate::lua::LuaObject> as std::iter::FromIterator<($crate::lua::LuaObject, $crate::lua::LuaObject)>>::from_iter([
                $(
                    (lua!($key), lua!($val))
                ),*
            ])
        )
    );
}

// TODO: Enumerate types for better error messages
#[derive(Clone, Debug)]
pub struct LuaTypeError {}

#[derive(Clone, Debug)]
pub enum LuaObject {
    Float(f32),
    String(CString),
    Unicode(String),
    Nil,
    Bool(bool),
    Table(LuaTable),
}

impl From<FromUtf8Error> for LuaTypeError {
    fn from(_err: FromUtf8Error) -> LuaTypeError {
        LuaTypeError {}
    }
}

impl From<IntoStringError> for LuaTypeError {
    fn from(_err: IntoStringError) -> LuaTypeError {
        LuaTypeError {}
    }
}

impl From<f32> for LuaObject {
    fn from(data: f32) -> LuaObject {
        LuaObject::Float(data)
    }
}

impl From<CString> for LuaObject {
    fn from(data: CString) -> LuaObject {
        LuaObject::String(data)
    }
}

impl From<String> for LuaObject {
    fn from(data: String) -> LuaObject {
        LuaObject::Unicode(data)
    }
}

impl From<Box<CStr>> for LuaObject {
    fn from(data: Box<CStr>) -> LuaObject {
        LuaObject::String(data.into_c_string())
    }
}

impl From<&str> for LuaObject {
    fn from(data: &str) -> LuaObject {
        LuaObject::Unicode(data.to_string())
    }
}

impl From<bool> for LuaObject {
    fn from(data: bool) -> LuaObject {
        LuaObject::Bool(data)
    }
}

impl Eq for LuaObject {}
impl PartialEq for LuaObject {
    fn eq(&self, other: &LuaObject) -> bool {
        use LuaObject::*;

        match (self, other) {
            (Float(f1), Float(f2)) => f1.eq(f2),
            // String comparisons
            (String(s1), String(s2)) => s1.eq(s2),
            (Unicode(s1), Unicode(s2)) => s1.eq(s2),
            (String(s1), Unicode(s2)) => s1.as_bytes().eq(s2.as_bytes()),
            (Unicode(s1), String(s2)) => s1.as_bytes().eq(s2.as_bytes()),
            // end string comparisons
            (Nil, Nil) => true,
            (Bool(b1), Bool(b2)) => b1.eq(b2),
            (Table(t1), Table(t2)) => t1.eq(t2),
            _ => false,
        }
    }
}

impl Hash for LuaObject {
    fn hash<H: Hasher>(&self, state: &mut H) {
        use LuaObject::*;

        match self {
            Float(f) => unsafe {
                let ptr = if f.is_nan() { &std::f32::NAN } else { f } as *const f32;
                state.write(&*(ptr as *const [u8; 4]))
            },
            String(s) => s.hash(state),
            Unicode(s) => s.hash(state),
            Nil => ().hash(state),
            Bool(b) => b.hash(state),
            Table(_) => panic!("Unhashable type 'table'"),
        }
    }
}

impl Display for LuaObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use LuaObject::*;

        match self {
            Float(f_) => Display::fmt(f_, f),
            String(s) => write!(f, "{}", s.to_string_lossy()),
            Unicode(s) => Display::fmt(s, f),
            Nil => f.write_str("Nil"),
            Bool(b) => Display::fmt(b, f),
            Table(ref t) => f
                .debug_map()
                .entries(
                    t.iter()
                        .map(|(k, v)| (DebugTableInner(k), DebugTableInner(v))),
                )
                .finish(),
        }
    }
}

/// Return a debug representation for displaying the value in a lua table. Tables are displayed
/// with the standard libraries `debug_map` formatter so the type we return needs to implement
/// debug, even though it will be used in a `Display` implementation.
struct DebugTableInner<'a>(&'a LuaObject);
impl std::fmt::Debug for DebugTableInner<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use LuaObject::*;

        match self.0 {
            String(ref i) => Debug::fmt(i, f),
            Unicode(ref i) => Debug::fmt(i, f),
            Bool(ref i) => Debug::fmt(i, f),
            _ => Display::fmt(self.0, f),
        }
    }
}

impl LuaObject {
    /// Evaluate the contents of this object.
    /// For String and Table, returns false if the object is empty.
    /// For Number returns false on `0`.
    /// For Bool returns the bool.
    /// For Nil returns false.
    pub fn evaluate_as_bool(&self) -> bool {
        use LuaObject::*;
        match self {
            Float(f) => *f == 0.0,
            String(s) => !s.as_bytes().is_empty(),
            Unicode(s) => !s.is_empty(),
            Nil => false,
            Bool(b) => *b,
            Table(t) => !t.is_empty(),
        }
    }

    pub fn as_float(&self) -> Result<f32, LuaTypeError> {
        match self {
            LuaObject::Float(f) => Ok(*f),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn into_string(self) -> Result<String, LuaTypeError> {
        match self {
            LuaObject::String(s) => Ok(s.into_string()?),
            LuaObject::Unicode(s) => Ok(s),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn to_string(&self) -> Result<String, LuaTypeError> {
        match self {
            LuaObject::String(s) => Ok(String::from_utf8(s.as_bytes().to_vec())?),
            LuaObject::Unicode(s) => Ok(s.clone()),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn to_string_lossy(&self) -> Result<String, LuaTypeError> {
        match self {
            LuaObject::String(s) => Ok(String::from_utf8_lossy(s.as_bytes()).to_string()),
            LuaObject::Unicode(s) => Ok(s.clone()),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn as_nil(&self) -> Result<(), LuaTypeError> {
        match self {
            LuaObject::Nil => Ok(()),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn as_bool(&self) -> Result<bool, LuaTypeError> {
        match self {
            LuaObject::Bool(b) => Ok(*b),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn into_hashmap(self) -> Result<LuaTable, LuaTypeError> {
        match self {
            LuaObject::Table(t) => Ok(t),
            _ => Err(LuaTypeError {}),
        }
    }

    pub fn as_hashmap(&self) -> Result<&LuaTable, LuaTypeError> {
        match self {
            LuaObject::Table(ref t) => Ok(t),
            _ => Err(LuaTypeError {}),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    use std::iter::FromIterator;

    #[test]
    fn test_macro_simple() {
        assert_eq!(lua!(10.), LuaObject::Float(10.));
        assert_eq!(
            lua!(cstr "foo"),
            LuaObject::String(CString::new("foo").unwrap())
        );
        assert_eq!(lua!("foo"), LuaObject::Unicode("foo".to_string()));
        assert_eq!(lua!(), LuaObject::Nil);
        assert_eq!(lua!(nil), LuaObject::Nil);
        assert_eq!(lua!(false), LuaObject::Bool(false));
        assert_eq!(lua!(10.), LuaObject::Float(10.));
    }

    #[test]
    fn test_macro_table_sugar() {
        let table = lua! {
            foo = 1.0,
            bar = 2.0 + 3.0
        };

        let expected = LuaObject::Table(OrderedHashMap::from_iter([
            (LuaObject::Unicode("foo".to_string()), LuaObject::Float(1.0)),
            (LuaObject::Unicode("bar".to_string()), LuaObject::Float(5.0)),
        ]));

        assert_eq!(table, expected);
    }

    #[test]
    fn test_macro_table_verbose() {
        let table = lua! {
            ["foo"] = "bar",
            [10.] = true,
            [false] = 100.
        };

        let expected = LuaObject::Table(OrderedHashMap::from_iter([
            (
                LuaObject::Unicode("foo".to_string()),
                LuaObject::Unicode("bar".to_string()),
            ),
            (LuaObject::Float(10.), LuaObject::Bool(true)),
            (LuaObject::Bool(false), LuaObject::Float(100.)),
        ]));

        assert_eq!(table, expected);
    }

    #[test]
    fn test_display() {
        assert_eq!(format!("{}", LuaObject::Float(100.)), "100");
        assert_eq!(
            format!(
                "{}",
                LuaObject::String(CString::new("Hello World").unwrap())
            ),
            "Hello World"
        );
        assert_eq!(
            format!("{}", LuaObject::Unicode("Hello World".into())),
            "Hello World"
        );
        assert_eq!(format!("{}", LuaObject::Nil), "Nil");
        assert_eq!(format!("{}", LuaObject::Bool(false)), "false");
        assert_eq!(format!("{}", LuaObject::Bool(true)), "true");
        assert_eq!(format!("{}", LuaObject::Table(LuaTable::new())), "{}");
        assert_eq!(format!("{}", LuaObject::Table(LuaTable::new())), "{}");
        // Complex nested table
        let mut table = LuaTable::new();
        table.insert(LuaObject::Nil, LuaObject::Float(100.));
        table.insert(LuaObject::Unicode("Foo Bar".into()), LuaObject::Bool(false));
        table.insert(
            LuaObject::String(CString::new("Hello World").unwrap()),
            LuaObject::Unicode("A new day".into()),
        );
        let mut inner_table = LuaTable::new();
        inner_table.insert(LuaObject::Float(1.), LuaObject::Bool(true));
        table.insert(LuaObject::Float(2.), LuaObject::Table(inner_table));
        assert_eq!(
            format!("{}", LuaObject::Table(table)),
            "{Nil: 100, \"Foo Bar\": false, \"Hello World\": \"A new day\", 2: {1: true}}"
        );
    }
}
