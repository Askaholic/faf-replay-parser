#![feature(test)]

extern crate test;

#[cfg(test)]
mod from_memory {
    use test::Bencher;

    use std::convert::TryFrom;
    use std::fs::File;
    use std::io::{BufReader, Read};

    use faf_replay_parser::iter::prelude::*;
    use faf_replay_parser::parser::parse_body_ticks;
    use faf_replay_parser::scfa::replay_command;
    use faf_replay_parser::scfa::*;
    use faf_replay_parser::{Parser, ParserBuilder, SCFA};

    const REPLAY_FILE: &str = "tests/data/6176549.scfareplay";

    fn get_replay_data() -> (usize, Vec<u8>) {
        let mut f = BufReader::new(File::open(REPLAY_FILE).expect("Couldn't open file"));
        let mut data = Vec::new();
        f.read_to_end(&mut data).expect("Couldn't read from file");

        let start = body_offset(&data).unwrap();
        (start, data)
    }

    /// Build a new parser and use it to fully parse one replay from memory.
    #[bench]
    fn build_and_parse_all(b: &mut Bencher) {
        let (_, data) = get_replay_data();

        // Create parser and parse replay
        b.iter(|| {
            let parser = ParserBuilder::<SCFA>::new().commands_all().build();
            let _replay = parser
                .parse(&mut data.as_slice())
                .expect("Failed to parse replay");
        });
    }

    fn bench_with_parser(parser: &Parser<SCFA>, b: &mut Bencher) {
        let (_, data) = get_replay_data();

        // Parse replay
        b.iter(|| {
            let _replay = parser
                .parse(&mut data.as_slice())
                .expect("Failed to parse replay");
        });
    }

    /// Fully parse one replay from memory. Reuses the same `Parser` for each iteration.
    #[bench]
    fn parse_all(b: &mut Bencher) {
        let parser = ParserBuilder::new().commands_all().build();

        bench_with_parser(&parser, b);
    }

    #[bench]
    fn parse_all_no_save_commands(b: &mut Bencher) {
        let parser = ParserBuilder::new()
            .commands_all()
            .save_commands(false)
            .build();

        bench_with_parser(&parser, b);
    }

    #[bench]
    fn parse_all_iter_no_save_commands(b: &mut Bencher) {
        use std::collections::HashMap;
        use ReplayCommand::*;

        let (start, data) = get_replay_data();

        b.iter(|| {
            let mut sim_tick = 0;
            let mut command_source = 0;
            let mut players_last_tick = HashMap::new();
            let mut checksum = [0; 16];
            let mut checksum_tick = None;
            let mut desync_tick = None;
            let mut desync_ticks = None;

            (&data[start..])
                .iter_commands::<SCFA>()
                .for_each(|command| match command.unwrap() {
                    // Copied contents of `process_command`
                    Advance { ticks } => sim_tick += ticks,
                    SetCommandSource { id } => command_source = id,
                    CommandSourceTerminated => {
                        players_last_tick.insert(command_source, sim_tick);
                    }
                    VerifyChecksum { digest, tick } => {
                        if checksum_tick < Some(tick) {
                            checksum_tick.replace(tick);
                            checksum.copy_from_slice(&digest[..16]);
                            return ();
                        }

                        if checksum != digest[..16] {
                            if desync_tick.is_none() {
                                desync_tick = Some(sim_tick);
                                desync_ticks = Some(vec![]);
                            }

                            desync_ticks.as_mut().unwrap().push(sim_tick);

                            panic!("Desynced at {}", sim_tick);
                        }
                    }
                    _ => (),
                });
        });
    }

    /// Extract the total tick count of a replay from memory. Reuses the same `Parser` for each
    /// iteration.
    #[bench]
    fn parse_ticks_with_parser(b: &mut Bencher) {
        let parser = ParserBuilder::new()
            .command(ReplayCommandId::try_from(replay_command::ADVANCE).unwrap())
            .build();

        bench_with_parser(&parser, b);
    }

    /// Extract the total tick count of a replay from memory without saving each parsed command
    /// to the `commands` Vec. Reuses the same `Parser` for each iteration.
    #[bench]
    fn parse_ticks_with_parser_no_save_commands(b: &mut Bencher) {
        let parser = ParserBuilder::new()
            .command(ReplayCommandId::try_from(replay_command::ADVANCE).unwrap())
            .save_commands(false)
            .build();

        bench_with_parser(&parser, b);
    }

    /// Extract the total tick count of a replay from memory using `body_ticks`
    #[bench]
    fn parse_ticks_fast(b: &mut Bencher) {
        let (start, data) = get_replay_data();
        let body = &data[start..];

        // Parse replay
        b.iter(|| {
            let _count = body_ticks(body).expect("Failed to parse replay");
        });
    }

    /// Extract the total tick count of a replay from memory using `parse_body_ticks`
    #[bench]
    fn parse_ticks_reader(b: &mut Bencher) {
        let (start, data) = get_replay_data();

        // Parse replay
        b.iter(|| {
            let _count =
                parse_body_ticks::<SCFA>(&mut &data[start..]).expect("Failed to parse replay");
        });
    }

    /// Extract the total tick count from a slice using a command frame iterator and iterator
    /// adapters.
    #[bench]
    fn parse_ticks_iter_raw(b: &mut Bencher) {
        use std::convert::TryInto;

        let (start, data) = get_replay_data();

        // Parse replay
        b.iter(|| {
            let _count: u32 = (&data[start..])
                .iter_command_frames_raw()
                .filter(|frame| frame.cmd == replay_command::ADVANCE)
                .map(|frame| u32::from_le_bytes(frame.data[3..7].try_into().unwrap()))
                .sum();
        });
    }

    /// Extract the total tick count from a slice using a command frame iterator and iterator
    /// adapters.
    #[bench]
    fn parse_ticks_iter(b: &mut Bencher) {
        use std::convert::TryInto;

        let (start, data) = get_replay_data();

        // Parse replay
        b.iter(|| {
            let _count: u32 = (&data[start..])
                .iter_command_frames::<SCFA>()
                .map(|r| r.unwrap())
                .filter(|frame| frame.cmd == replay_command::ADVANCE)
                .map(|frame| u32::from_le_bytes(frame.data[3..7].try_into().unwrap()))
                .sum();
        });
    }

    #[bench]
    fn parse_ticks_iter_while_ok(b: &mut Bencher) {
        use std::convert::TryInto;

        let (start, data) = get_replay_data();

        // Parse replay
        b.iter(|| {
            let _count: u32 = (&data[start..])
                .iter_command_frames::<SCFA>()
                // Use while_ok to unwrap for us
                .while_ok()
                .filter(|frame| frame.cmd == replay_command::ADVANCE)
                .map(|frame| u32::from_le_bytes(frame.data[3..7].try_into().unwrap()))
                .sum();
        });
    }
}
